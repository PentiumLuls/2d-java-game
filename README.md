# What's that sir?
Glad you asked. It is 2D tile-based game, which will support multiplayer.
Possibly, an RPG one.

# Setup 
1. Install Java 17
```bash
 sudo apt install openjdk-17-jre -y
 sudo apt install openjdk-17-jdk -y
```

# Run
## Docker
Run all services:
```bash
docker-compose up -d --build
```
Run specific service(for ex, frontend for web-addition):
```bash
docker-compose up -d --build angular-nginx
```

# Now what?
1. Checkout issues, milestones tabs in gitlab
2. Get understanding of architecture [here](doc/ClientArchitecture.drawio).
3. Read other [documentation](doc)