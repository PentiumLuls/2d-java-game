# Tile
There are two types of Tiles: "basic" and "layered".

**Basic** - are instances of `Tile` class.

**Layered** - are wrappers around basic tiles, which can be placed over basic one
in order to create "depth" effect. For, ex: Wall(which sprite contains transparent pixels) 
can be placed over the grass or grass path. More info below.

## Layered Tiles
As said above, it's wrapper around `Tile` class. It has coordinates, to tell where it placed.

While basic tiles stored in two-dimensional array, layered stored in plain array, 
because it does not have to be in each cell on the map.

## Image
Tile images stored in `resources/tiles`. It can be:
 - single 16x16 image,
 - spritesheet consisting of many 16x16 sprites(total dimensions should be divisible by 16)

Later in game it will ve scaled to needed size.

##  Variations
Each tile can have 1-n variations(different sprites, but same behavior).
See `stateId` field in `Tile`. 
`stateId` corresponds to index of sprite in sheet. Example of how index counted:
![tile_state_example.png](tile_state_example.png)

For reference, see `GrassVariations` enum with different states for grass.

## Collision


## Troubleshoot
1. Image is corrupted when loaded in game - possibly issue in image color model. 
As workaround try opening image in popular editor(photoshop) and saving it.
