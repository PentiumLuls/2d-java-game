# 1. Those were days...
## Gameplay
 - farming
 - mining?
 - quests for hunting slimes, weak monsters

## Mission
 - prepare player for hard battles - knowledge of fight abilities and core mechanics
 - make player love chilly days - lovely npc's, own farm and house

## Locations
 - starting village
 - near village woods/fields

## Plot
You settled in this cozy village because (...)
Here you found friendly villagers and other similar to you settlers

----

# 2. The end and the beginning
## Gameplay
 - fighting

## Mission
 - add action
 - add a few new fighting mechanics ???

## Locations
 - starting village
 - another bigger town

## Plot
Suddenly, has come an end of peaceful days. War has come.
