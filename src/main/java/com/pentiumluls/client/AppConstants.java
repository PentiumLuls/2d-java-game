package com.pentiumluls.client;

public class AppConstants {
    // SCREEN SETTINGS
    public static final int ORIGINAL_TILE_SIZE = 16; // 16x16 pixel for each tile. For tiles image parsing
    public static final int SCALE = 3;
    public static final int TILE_SIZE = ORIGINAL_TILE_SIZE * SCALE; // 48x48 pixels
    public static final int HELD_ITEM_SIZE = ORIGINAL_TILE_SIZE * 2; // 32x32 pixels
    public static final int MAX_SCREEN_COL = 20; // how many tiles are fit to screen
    public static final int MAX_SCREEN_ROW = 14; // how many tiles are fit to screen
    public static final int SCREEN_WIDTH = TILE_SIZE * MAX_SCREEN_COL; // 768 pixels
    public static final int SCREEN_HEIGHT = TILE_SIZE * MAX_SCREEN_ROW; // 576 pixels

    public static final int SELECTOR_HEIGHT = TILE_SIZE + 5; // with border. Height of inventory hotbar
    public static final int TILES_IN_SELECTOR = 18; // visible tile types in selector
    public static final int SELECTOR_BORDER = 3;

    public static final int FPS = 60;

    public static String SAVE_DIR = System.getProperty("user.home") + "/.pentiumluls/";
    public static String MAPS_DIR = SAVE_DIR + "maps/";

    public static String HOST = "localhost";
    public static int PORT = 6666;

    public static final int UNAUTHORIZED_PLAYER_ID = -1;

}
