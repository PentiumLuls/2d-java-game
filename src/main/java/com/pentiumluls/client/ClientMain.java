package com.pentiumluls.client;

/** entrypoint to client part of game */
public class ClientMain {

    public static void main(String[] args) {
        Core core = new Core();
        core.start();
    }
}
