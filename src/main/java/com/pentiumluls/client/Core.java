package com.pentiumluls.client;

public class Core {

    public void start() {
        DataLayer dataLayer = new DataLayer();

        NetworkLayer networkLayer = new NetworkLayer(AppConstants.HOST, AppConstants.PORT, dataLayer);
        Thread connectionThread = new Thread(networkLayer);
        connectionThread.start();

        LogicLayer logicLayer = new LogicLayer(dataLayer);
        Thread logicThread = new Thread(logicLayer);
        logicThread.start();

        EventPool eventPool = new EventPool(networkLayer, logicLayer);

        UILayer uiLayer = new UILayer(dataLayer);
    }
}
