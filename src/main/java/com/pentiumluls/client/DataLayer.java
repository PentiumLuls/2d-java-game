package com.pentiumluls.client;

import com.pentiumluls.client.data.GameState;
import com.pentiumluls.common.data.PlayerInventory;
import com.pentiumluls.common.data.enemy.Enemy;
import com.pentiumluls.common.data.map.WorldMap;
import com.pentiumluls.common.net.transferdata.PlayerTransferData;
import com.pentiumluls.client.data.entity.Player;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class DataLayer {
    public GameState gameState;
    public Player player;
    public Map<Integer, Player> players; // other players. Integer = playerId
    public PlayerInventory inventory;
    public Map<Integer, Enemy> enemies; // Integer = enemy id

    public WorldMap map;
    public int mapWidth;
    public int mapHeight;

    public int hoveredTileX;
    public int hoveredTileY;

    public boolean isAuthRequestInProcess = false;

    public DataLayer() {
        player = new Player();
        // TODO: remove stubs
        player.id = AppConstants.UNAUTHORIZED_PLAYER_ID;
        player.username = "";

        players = new HashMap<>();
        enemies = new HashMap<>();
    }

    public void addOnlinePlayer(Player player) {
        players.put(player.id, player);
    }

    public Player getOnlinePlayer(int id) {
        return players.get(id);
    }

    public List<Player> getOnlinePlayers() {
        return players.values().stream().toList();
    }

    public void setOnlinePlayers(List<PlayerTransferData> onlinePlayers) {
        players = onlinePlayers.stream()
                .map(Player::new)
                .collect(Collectors.toMap(Player::getId, p -> p));
    }

    public void removePlayer(int playerId) {
        players.remove(playerId);
    }

    public void setMap(WorldMap map) {
        this.map = map;
        mapWidth = map.tiles.length;
        mapHeight = map.tiles[0].length;
    }

    public void setEnemies(Map<Integer, Enemy> enemies) {
        this.enemies = enemies;
    }

    public List<Enemy> getEnemies() {
        return enemies.values().stream().toList();
    }
}
