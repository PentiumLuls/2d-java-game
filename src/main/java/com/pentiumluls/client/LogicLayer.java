package com.pentiumluls.client;

import com.pentiumluls.client.command.MouseLeftClickCommand;
import com.pentiumluls.client.command.MouseRightClickCommand;
import com.pentiumluls.client.command.MouseWheelScrolledCommand;
import com.pentiumluls.client.data.GameState;
import com.pentiumluls.client.event.logic.AuthorizeEvent;
import com.pentiumluls.client.event.logic.PlaceItemEvent;
import com.pentiumluls.client.io.KeyHandler;
import com.pentiumluls.client.io.MouseHandler;
import com.pentiumluls.client.io.MouseWheelHandler;
import com.pentiumluls.client.logic.*;
import com.pentiumluls.common.util.logger.Logger;
import com.pentiumluls.common.util.logger.LoggerLayer;

public class LogicLayer implements Runnable {
    private DataLayer dataLayer;
    private KeyHandler keyHandler;
    private MouseWheelHandler mouseWheelHandler;
    private MouseHandler mouseHandler;

    private ServerEventLogic serverEventLogic;
    private PlayerLogic playerLogic;
    private MapLogic mapLogic;
    private InventoryLogic inventoryLogic;
    public CollisionHelper collisionHelper;

    public LogicLayer(DataLayer dataLayer) {
        this.dataLayer = dataLayer;
        keyHandler = KeyHandler.instance();
        mouseWheelHandler = MouseWheelHandler.instance().setCommand(new MouseWheelScrolledCommand(this));
        mouseHandler = MouseHandler.instance()
                .setLeftClickCommand(new MouseLeftClickCommand(this))
                .setRightClickCommand(new MouseRightClickCommand(this));
        playerLogic = new PlayerLogic(dataLayer, keyHandler);
        mapLogic = new MapLogic(dataLayer);
        inventoryLogic = new InventoryLogic(dataLayer);
        serverEventLogic = new ServerEventLogic(dataLayer, playerLogic, mapLogic, inventoryLogic);
        collisionHelper = new CollisionHelper(dataLayer);

        dataLayer.gameState = GameState.LOGIN;
    }

    @Override
    public void run() {
        double drawInterval = 1000000000 / AppConstants.FPS;
        double delta = 0;
        long lastTime = System.nanoTime();
        long currentTime;
        long timer = 0;
        int drawCount = 0;

        while (true) {
            currentTime = System.nanoTime();
            delta += (currentTime - lastTime) / drawInterval;
            timer += (currentTime - lastTime);
            lastTime = currentTime;

            if (delta >= 1) {
                // ACTUAL GAME LOOP, FRAMED IN PREDEFINED FPS
                update();

                delta--;
                drawCount++;
            }
            if (timer >= 1000000000) {
                System.out.println("FPS: " + drawCount);
                drawCount = 0;
                timer = 0;
            }
        }
    }

    private void update() {
        EventPool.readLogicEvents()
                .forEach(serverEventLogic::handleEvent);

        if (dataLayer.gameState == GameState.LOGIN) {
            if (!dataLayer.isAuthRequestInProcess && !dataLayer.player.username.isEmpty())
                authorize();
            return;

        } else if (dataLayer.gameState != GameState.PLAY)
            return;

        // ACTUAL GAME LOGIC
        playerLogic.update();
        collisionHelper.checkTileCollision(dataLayer.player, dataLayer.player.direction);
    }

    private void authorize() {
        log("Authorizing...");
        EventPool.writeNetworkEvent(new AuthorizeEvent(dataLayer.player.id, dataLayer.player.username));
        dataLayer.isAuthRequestInProcess = true;
    }

    /** triggered by MouseWheelHandler */
    public void onScrollUp() {
        inventoryLogic.selectNextItem();
    }

    /** triggered by MouseWheelHandler */
    public void onScrollDown() {
        inventoryLogic.selectPreviousItem();
    }

    public void onLeftClick() {
        if (dataLayer.gameState != GameState.PLAY)
            return;
    }

    public void onRightClick() {
        if (dataLayer.gameState != GameState.PLAY)
            return;

        dataLayer.inventory.getCurrentItem()
                .filter(item -> item.type.isPlaceAble())
                .ifPresent(item -> EventPool.writeNetworkEvent(new PlaceItemEvent(item, dataLayer.hoveredTileX, dataLayer.hoveredTileY)));
    }

    private void log(String message) {
        Logger.log(LoggerLayer.LOGIC, message);
    }
}
