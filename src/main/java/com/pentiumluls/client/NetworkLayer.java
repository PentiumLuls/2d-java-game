package com.pentiumluls.client;

import com.pentiumluls.client.event.Event;
import com.pentiumluls.client.event.logic.AuthorizeEvent;
import com.pentiumluls.client.event.logic.MoveEvent;
import com.pentiumluls.client.event.logic.PlaceItemEvent;
import com.pentiumluls.client.event.network.*;
import com.pentiumluls.common.net.packets.*;
import com.pentiumluls.common.util.logger.Logger;
import com.pentiumluls.common.util.logger.LoggerLayer;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

public class NetworkLayer implements Runnable {
    private DataLayer dataLayer;
    private int port;
    private Socket socket;
    private ObjectInputStream inStream;
    private ObjectOutputStream outStream;

    public NetworkLayer(String host, int port, DataLayer dataLayer) {
        this.dataLayer = dataLayer;
        this.port = port;
        try {
            socket = new Socket(host, port);
            outStream = new ObjectOutputStream(socket.getOutputStream());
            inStream = new ObjectInputStream(socket.getInputStream());
        } catch (IOException e) {
            closeEverything(socket, inStream, outStream);
        }
    }

    @Override
    public void run() {
        listenPackets();
    }

    public void listenPackets() {
        new Thread(() -> {
            Object packet;
            log("start listening packets from server");

            while (socket.isConnected()) {
                try {
                    packet = inStream.readObject();
                    handlePacket(packet);
                } catch (IOException e) {
                    closeEverything(socket, inStream, outStream);
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    public void sendPacket(Packet packet) {
        try {
            outStream.writeObject(packet);
            outStream.flush();

        } catch (IOException e) {
            closeEverything(socket, inStream, outStream);
        }
    }

    /** handle packets from server */
    public void handlePacket(Object p) {
        if (p instanceof ChatMessagePacket packet) {
            log(packet.userName + ": " + packet.message);

        } else if (p instanceof PlayerSuccessfullyJoinedPacket packet) {
            log("Successfully joined, id=" + packet.player.id + "  players online " + packet.onlinePlayers.size());
            EventPool.writeLogicEvent(new SuccessfulAuthEvent(packet.player, packet.onlinePlayers, packet.map, packet.inventory, packet.enemies));

        } else if (p instanceof PlayerJoinedPacket packet) { // Another player joined
            log("Another player successfully joined, id=" + packet.player.id);
            EventPool.writeLogicEvent(new PlayerJoinedEvent(packet.player));

        } else if (p instanceof PlayerDisconnectPacket packet) { // another player disconnected
            EventPool.writeLogicEvent(new PlayerDisconnectedEvent(packet.playerId));

        } else if (p instanceof PlayerMovedPacket packet) {
            EventPool.writeLogicEvent(new PlayerMovedEvent(packet.playerId, packet.direction, packet.worldX, packet.worldY));

        } else if (p instanceof PlayerPickedObjectPacket packet) {
            EventPool.writeLogicEvent(new PlayerPickedObjectEvent(packet.playerId, packet.item, packet.amount, packet.x, packet.y));

        } else if (p instanceof PlaceItemSuccessPacket packet) {
            EventPool.writeLogicEvent(new PlaceItemSuccessEvent(packet.playerId, packet.item, packet.object, packet.worldX, packet.worldY));

        } else if (p instanceof NewDayPacket packet) {
            EventPool.writeLogicEvent(new NewDayEvent(packet.changedObjects));
        }
    }

    /** handle events from other client's layers and send appropriate packets to server */
    public void handleEvent(Event e) {
        if (e instanceof AuthorizeEvent event)
            sendPacket(new PlayerAuthPacket(event.username, event.id));

        else if (e instanceof MoveEvent event)
            sendPacket(new PlayerMovePacket(dataLayer.player.id, event.direction));

        else if (e instanceof PlaceItemEvent event) {
            sendPacket(new PlaceItemPacket(event.currentItem, event.worldX, event.worldY));
        }
    }

    private void closeEverything(Socket socket, ObjectInputStream reader, ObjectOutputStream writer) {
        try {
            if (reader != null)
                reader.close();

            if (writer != null)
                writer.close();

            if (socket != null)
                socket.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void log(String message) {
        Logger.log(LoggerLayer.NETWORK, message);
    }
}
