package com.pentiumluls.client;

import com.pentiumluls.client.ui.GamePanel;

import javax.swing.*;

public class UILayer {

    public UILayer(DataLayer dataLayer) {
        JFrame window = new JFrame();
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        window.setResizable(false);
        window.setTitle("Ye olde game");

        GamePanel gamePanel = new GamePanel(dataLayer);
        window.add(gamePanel);

        window.pack(); // set main window size to fit subcomponents

        window.setLocationRelativeTo(null);
        window.setVisible(true);

        gamePanel.startGameThread();
    }
}
