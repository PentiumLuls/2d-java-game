package com.pentiumluls.client.command;

import com.pentiumluls.client.LogicLayer;
import com.pentiumluls.common.logic.Command;

public class MouseLeftClickCommand implements Command {
    private LogicLayer logicLayer;

    public MouseLeftClickCommand(LogicLayer logicLayer) {
        this.logicLayer = logicLayer;
    }

    @Override
    public void execute() {
        logicLayer.onLeftClick();
    }
}
