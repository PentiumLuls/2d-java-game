package com.pentiumluls.client.command;

import com.pentiumluls.client.LogicLayer;
import com.pentiumluls.common.logic.Command;

public class MouseRightClickCommand implements Command {
    private LogicLayer logicLayer;

    public MouseRightClickCommand(LogicLayer logicLayer) {
        this.logicLayer = logicLayer;
    }

    @Override
    public void execute() {
        logicLayer.onRightClick();
    }
}
