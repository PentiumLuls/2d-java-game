package com.pentiumluls.client.command;

import com.pentiumluls.client.LogicLayer;
import com.pentiumluls.common.logic.Command;

public class MouseWheelScrolledCommand implements Command {
    private boolean isUp;
    private LogicLayer logicLayer;

    public MouseWheelScrolledCommand(LogicLayer logicLayer) {
        this.logicLayer = logicLayer;
    }

    @Override
    public void execute() {
        if (isUp)
            logicLayer.onScrollUp();
        else
            logicLayer.onScrollDown();
    }

    public MouseWheelScrolledCommand onScrollUp() {
        this.isUp = true;
        return this;
    }

    public MouseWheelScrolledCommand onScrollDown() {
        this.isUp = false;
        return this;
    }
}
