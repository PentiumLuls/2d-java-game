package com.pentiumluls.client.command;

import com.pentiumluls.client.ui.LoginUI;
import com.pentiumluls.common.logic.Command;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class SubmitInputCommand implements Command {
    private LoginUI loginUI;

    @Override
    public void execute() {
        loginUI.submitLogin();
    }
}
