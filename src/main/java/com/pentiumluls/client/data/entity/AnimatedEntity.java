package com.pentiumluls.client.data.entity;

import java.awt.image.BufferedImage;
import java.util.List;

public class AnimatedEntity extends Entity {
    // TODO: use list for each direction? Because entity can have 1 or 3+ sprites
    public transient List<BufferedImage> upSprites, downSprites, leftSprites, rightSprites;
    public transient List<BufferedImage> standSprites; // stand still
    public transient int spriteCounter = 0; // Used to delay sprite changing
    public transient int spriteChangingSpeed = 10;
    public transient int spriteIndex = 0; // index of current sprite to display from sprites list
}
