package com.pentiumluls.client.data.entity;

import com.pentiumluls.common.data.Direction;

import java.awt.*;
import java.io.Serializable;

public class Entity implements Serializable {
    public int worldX, worldY;
    public int speed;
    public int speedBonus = 0;
    public Direction direction;
    public boolean isMoving = false; // false if standing still

    public Rectangle collisionBox; // relative to original entity sprite rectangle
    public boolean collisionEnabled = false; // collision is not calculated if false
    public boolean collisionOn = false; // whether it collides with sprites in current direction

    public void addSpeedBonus(int speedBonus) {
        this.speedBonus += speedBonus;
    }
}
