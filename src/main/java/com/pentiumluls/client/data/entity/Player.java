package com.pentiumluls.client.data.entity;

import com.pentiumluls.common.data.Direction;
import com.pentiumluls.common.net.transferdata.PlayerTransferData;

import java.awt.*;
import java.io.Serializable;

public class Player extends AnimatedEntity implements Serializable {
    public int id;
    public String username;

    public int screenX;
    public int screenY;
    public transient Direction lastHorizontalDirection; // can only be LEFT or RIGHT. used for animation when direction is UP or DOWN
    public int spriteScale = 2; // Change collisionBox if changing scale :)

    public Player() {
    }

    public Player(PlayerTransferData player) {
        setValuesFrom(player);
    }

    public void setValuesFrom(PlayerTransferData player) {
        this.id = player.id;
        this.username = player.name;
        this.worldX = player.worldX;
        this.worldY = player.worldY;

        // TODO: remove hardcode
        collisionBox = new Rectangle(15, 28, 22, 36);
        collisionEnabled = true;
        spriteChangingSpeed = 5;
        speed = 4;
        direction = Direction.DOWN;
        isMoving = false;
    }

    public int getId() {
        return id;
    }
}
