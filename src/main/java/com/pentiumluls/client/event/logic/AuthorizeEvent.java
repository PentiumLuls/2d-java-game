package com.pentiumluls.client.event.logic;

import com.pentiumluls.client.event.Event;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class AuthorizeEvent extends Event {
    public int id; // for now can be -1, in case user is joining first time
    public String username;
}
