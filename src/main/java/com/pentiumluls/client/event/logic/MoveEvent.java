package com.pentiumluls.client.event.logic;

import com.pentiumluls.client.event.Event;
import com.pentiumluls.common.data.Direction;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class MoveEvent extends Event {
    public Direction direction;
}
