package com.pentiumluls.client.event.logic;

import com.pentiumluls.client.event.Event;
import com.pentiumluls.common.data.item.Item;
import lombok.AllArgsConstructor;
import org.jetbrains.annotations.Nullable;

@AllArgsConstructor
public class PlaceItemEvent extends Event {
    public Item currentItem;
    public int worldX; // tile indexes, not in pixels
    public int worldY;
}
