package com.pentiumluls.client.event.network;

import com.pentiumluls.client.event.Event;
import com.pentiumluls.common.data.Pair;
import com.pentiumluls.common.data.object.Object;
import lombok.AllArgsConstructor;

import java.util.HashMap;

@AllArgsConstructor
public class NewDayEvent extends Event {
    public HashMap<Pair<Integer, Integer>, Object> changedObjects;
}
