package com.pentiumluls.client.event.network;

import com.pentiumluls.client.event.Event;
import com.pentiumluls.common.data.item.Item;
import com.pentiumluls.common.data.object.Object;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class PlaceItemSuccessEvent extends Event {
    public int playerId; // player that placed
    public Item item; // item that was placed
    public Object object; // object that is a result of item placement
    public int worldX; // tile indexes, not in pixels
    public int worldY;
}
