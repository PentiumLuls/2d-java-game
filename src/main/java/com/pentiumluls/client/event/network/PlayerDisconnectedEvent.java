package com.pentiumluls.client.event.network;

import com.pentiumluls.client.event.Event;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class PlayerDisconnectedEvent extends Event {
    public int playerId;
}
