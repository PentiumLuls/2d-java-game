package com.pentiumluls.client.event.network;

import com.pentiumluls.client.event.Event;
import com.pentiumluls.common.net.transferdata.PlayerTransferData;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class PlayerJoinedEvent extends Event {
    public PlayerTransferData player;
}
