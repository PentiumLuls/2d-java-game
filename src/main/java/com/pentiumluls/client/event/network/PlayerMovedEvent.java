package com.pentiumluls.client.event.network;

import com.pentiumluls.client.event.Event;
import com.pentiumluls.common.data.Direction;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class PlayerMovedEvent extends Event {
    public int playerId;
    public Direction direction;
    public int worldX;
    public int worldY;
}
