package com.pentiumluls.client.event.network;

import com.pentiumluls.client.event.Event;
import com.pentiumluls.common.data.item.Item;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class PlayerPickedObjectEvent extends Event {
    public int playerId;
    public Item item;
    public int amount;
    public int x;
    public int y;
}
