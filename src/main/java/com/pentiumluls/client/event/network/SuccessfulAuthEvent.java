package com.pentiumluls.client.event.network;

import com.pentiumluls.client.event.Event;
import com.pentiumluls.common.data.PlayerInventory;
import com.pentiumluls.common.data.enemy.Enemy;
import com.pentiumluls.common.data.map.WorldMap;
import com.pentiumluls.common.net.transferdata.PlayerTransferData;
import lombok.AllArgsConstructor;

import java.util.List;
import java.util.Map;

@AllArgsConstructor
public class SuccessfulAuthEvent extends Event {
    public PlayerTransferData player;
    public List<PlayerTransferData> onlinePlayers;
    public WorldMap map;
    public PlayerInventory inventory;
    public Map<Integer, Enemy> enemies;
}
