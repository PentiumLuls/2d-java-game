package com.pentiumluls.client.io;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class KeyHandler implements KeyListener {
    private static KeyHandler instance;

    public boolean upPressed = false, downPressed = false, leftPressed = false, rightPressed = false;
    public boolean debugKeyPressed = false;

    public static KeyHandler instance() {
        if (instance == null)
            instance = new KeyHandler();

        return instance;
    }

    @Override
    public void keyTyped(KeyEvent keyEvent) {
    }

    @Override
    public void keyPressed(KeyEvent keyEvent) {
        int code = keyEvent.getKeyCode();
        switch (code) {
            case KeyEvent.VK_W -> upPressed = true;
            case KeyEvent.VK_S -> downPressed = true;
            case KeyEvent.VK_A -> leftPressed = true;
            case KeyEvent.VK_D -> rightPressed = true;
            case KeyEvent.VK_Q -> debugKeyPressed = true;
            case KeyEvent.VK_F9 -> saveKeyPressed();
        }
    }

    @Override
    public void keyReleased(KeyEvent keyEvent) {
        int code = keyEvent.getKeyCode();
        switch (code) {
            case KeyEvent.VK_W -> upPressed = false;
            case KeyEvent.VK_S -> downPressed = false;
            case KeyEvent.VK_A -> leftPressed = false;
            case KeyEvent.VK_D -> rightPressed = false;
            case KeyEvent.VK_Q -> debugKeyPressed = false;
        }
    }

    private void saveKeyPressed() {
//        gamePanel.saveKeyPressed();
    }
}
