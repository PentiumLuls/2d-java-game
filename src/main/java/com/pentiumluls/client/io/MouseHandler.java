package com.pentiumluls.client.io;

import com.pentiumluls.common.logic.Command;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class MouseHandler implements MouseListener {
    private static MouseHandler instance;
    Command leftClickCommand;
    Command rightClickCommand;

    public boolean isLeftClicked = false;
    public boolean isMiddleClicked = false;
    public boolean isRightClicked = false;

    private MouseHandler() {
    }

    public static MouseHandler instance() {
        if (instance == null)
            instance = new MouseHandler();
        return instance;
    }

    public MouseHandler setLeftClickCommand(Command leftClickCommand) {
        this.leftClickCommand = leftClickCommand;
        return this;
    }

    public MouseHandler setRightClickCommand(Command rightClickCommand) {
        this.rightClickCommand = rightClickCommand;
        return this;
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        // nothing
    }

    @Override
    public void mousePressed(MouseEvent e) {
        switch (e.getButton()) {
            case MouseEvent.BUTTON1 ->isLeftClicked = true;
            case MouseEvent.BUTTON2 ->isMiddleClicked = true;
            case MouseEvent.BUTTON3 ->isRightClicked = true;
        }
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        switch (e.getButton()) {
            case MouseEvent.BUTTON1 -> {
                isLeftClicked = false;
                leftClickCommand.execute();
            }
            case MouseEvent.BUTTON2 ->isMiddleClicked = false;
            case MouseEvent.BUTTON3 -> {
                isRightClicked = false;
                rightClickCommand.execute();
            }
        }
    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }
}
