package com.pentiumluls.client.io;

import com.pentiumluls.client.command.MouseWheelScrolledCommand;

import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;

/** used in logic, but initialised in UI, because of MouseWheelListener specific */
public class MouseWheelHandler implements MouseWheelListener {
    private MouseWheelScrolledCommand command;
    private static MouseWheelHandler instance;

    private MouseWheelHandler() {
    }

    public MouseWheelHandler setCommand(MouseWheelScrolledCommand command) {
        this.command = command;
        return this;
    }

    public static MouseWheelHandler instance() {
        if (instance == null)
            instance = new MouseWheelHandler();
        return instance;
    }

    @Override
    public void mouseWheelMoved(MouseWheelEvent e) {
        if (command == null)
            return;

        if (e.getWheelRotation() < 0) {
            command.onScrollUp()
                    .execute();
        } else {
            command.onScrollDown()
                    .execute();
        }
    }
}
