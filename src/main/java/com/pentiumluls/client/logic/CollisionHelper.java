package com.pentiumluls.client.logic;

import com.pentiumluls.client.AppConstants;
import com.pentiumluls.client.DataLayer;
import com.pentiumluls.common.data.Direction;
import com.pentiumluls.common.data.Pair;
import com.pentiumluls.common.data.map.LayeredTile;
import com.pentiumluls.common.data.map.Tile;
import com.pentiumluls.client.data.entity.Player;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class CollisionHelper {
    private DataLayer dataLayer;

    /** If player is colliding with any tile on it's movement direction -
     *  prevent player from moving further in same direction by setting "player.collisionOn" */
    public void checkTileCollision(Player player, Direction direction) {
        player.collisionOn = false; // reset

        int entityLeftWorldX = player.worldX + player.collisionBox.x;
        int entityRightWorldX = player.worldX + player.collisionBox.x + player.collisionBox.width;
        int entityTopWorldY = player.worldY + player.collisionBox.y;
        int entityBottomWorldY = player.worldY + player.collisionBox.y + player.collisionBox.height;

        int entityLeftCol = entityLeftWorldX / AppConstants.TILE_SIZE;
        int entityRightCol = entityRightWorldX / AppConstants.TILE_SIZE;
        int entityTopRow = entityTopWorldY / AppConstants.TILE_SIZE;
        int entityBottomRow = entityBottomWorldY / AppConstants.TILE_SIZE;

        Tile tile1, tile2; // we need to check only 2 tiles in direction of player view
        LayeredTile layeredTile1, layeredTile2; // we need to check only 2 tiles in direction of player view
        switch (player.direction) {
            case UP -> {
                //check basic tiles
                entityTopRow = (entityTopWorldY - (player.speed + player.speedBonus)) / AppConstants.TILE_SIZE;
                tile1 = dataLayer.map.tiles[entityLeftCol][entityTopRow];
                tile2 = dataLayer.map.tiles[entityRightCol][entityTopRow];
                if (tile1.type.isCollision() || tile2.type.isCollision()) {
                    player.collisionOn = true;
                }
                //check layered tiles
                layeredTile1 = dataLayer.map.layeredTiles.get(new Pair<>(entityLeftCol, entityTopRow));
                layeredTile2 = dataLayer.map.layeredTiles.get(new Pair<>(entityRightCol, entityTopRow));
                if (layeredTile1 != null && layeredTile1.type.isCollision() || layeredTile2 != null && layeredTile2.type.isCollision()) {
                    player.collisionOn = true;
                }
            }
            case DOWN -> {
                //check basic tiles
                entityBottomRow = (entityBottomWorldY + (player.speed + player.speedBonus)) / AppConstants.TILE_SIZE;
                tile1 = dataLayer.map.tiles[entityLeftCol][entityBottomRow];
                tile2 = dataLayer.map.tiles[entityRightCol][entityBottomRow];
                if (tile1.type.isCollision() || tile2.type.isCollision()) {
                    player.collisionOn = true;
                }
                //check layered tiles
                layeredTile1 = dataLayer.map.layeredTiles.get(new Pair<>(entityLeftCol, entityBottomRow));
                layeredTile2 = dataLayer.map.layeredTiles.get(new Pair<>(entityRightCol, entityBottomRow));
                if (layeredTile1 != null && layeredTile1.type.isCollision() || layeredTile2 != null && layeredTile2.type.isCollision()) {
                    player.collisionOn = true;
                }
            }
            case LEFT -> {
                //check basic tiles
                entityLeftCol = (entityLeftWorldX - (player.speed + player.speedBonus)) / AppConstants.TILE_SIZE;
                tile1 = dataLayer.map.tiles[entityLeftCol][entityTopRow];
                tile2 = dataLayer.map.tiles[entityLeftCol][entityBottomRow];
                if (tile1.type.isCollision() || tile2.type.isCollision()) {
                    player.collisionOn = true;
                }
                //check layered tiles
                layeredTile1 = dataLayer.map.layeredTiles.get(new Pair<>(entityLeftCol, entityTopRow));
                layeredTile2 = dataLayer.map.layeredTiles.get(new Pair<>(entityLeftCol, entityBottomRow));
                if (layeredTile1 != null && layeredTile1.type.isCollision() || layeredTile2 != null && layeredTile2.type.isCollision()) {
                    player.collisionOn = true;
                }
            }
            case RIGHT -> {
                //check basic tiles
                entityRightCol = (entityRightWorldX + (player.speed + player.speedBonus)) / AppConstants.TILE_SIZE;
                tile1 = dataLayer.map.tiles[entityRightCol][entityTopRow];
                tile2 = dataLayer.map.tiles[entityRightCol][entityBottomRow];
                if (tile1.type.isCollision() || tile2.type.isCollision()) {
                    player.collisionOn = true;
                }
                //check layered tiles
                layeredTile1 = dataLayer.map.layeredTiles.get(new Pair<>(entityRightCol, entityTopRow));
                layeredTile2 = dataLayer.map.layeredTiles.get(new Pair<>(entityRightCol, entityBottomRow));
                if (layeredTile1 != null && layeredTile1.type.isCollision() || layeredTile2 != null && layeredTile2.type.isCollision()) {
                    player.collisionOn = true;
                }
            }
        }
    }

    /** Returns true if entity is colliding with object.
     *  Also, if object has collision enabled,
     *  prevent entity from moving further towards object by setting "entity.collisionOn" */
    public boolean checkObjectCollision(Player entity, Object object) {
//        int entityCollisionX = entity.worldX + entity.collisionBox.x;
//        int entityCollisionY = entity.worldY + entity.collisionBox.y;
//        Rectangle entityCollisionBox = new Rectangle(entityCollisionX, entityCollisionY,
//                entity.collisionBox.width, entity.collisionBox.height);
//
//        int objectCollisionX = object.worldX + object.collisionBox.x;
//        int objectCollisionY = object.worldY + object.collisionBox.y;
//        Rectangle objectCollisionBox = new Rectangle(objectCollisionX, objectCollisionY,
//                object.collisionBox.width, object.collisionBox.height);
//
//        switch (entity.direction) { // predict entity collision coordinates based on movement direction
//            case UP -> entityCollisionBox.y -= entity.speed;
//            case DOWN -> entityCollisionBox.y += entity.speed;
//            case LEFT -> entityCollisionBox.x -= entity.speed;
//            case RIGHT -> entityCollisionBox.x += entity.speed;
//        }
//
//        if (entityCollisionBox.intersects(objectCollisionBox)) {
//            if (objectsHelper.hasCollision(object)) {
//                entity.collisionOn = true;
//            }
//            return true;
//        }
        return false;
    }
}
