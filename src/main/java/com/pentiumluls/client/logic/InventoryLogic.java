package com.pentiumluls.client.logic;

import com.pentiumluls.client.AppConstants;
import com.pentiumluls.client.DataLayer;
import com.pentiumluls.common.data.PlayerInventory;
import com.pentiumluls.common.data.item.Item;
import com.pentiumluls.common.exception.CanNotRemoveInventoryItem;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class InventoryLogic {
    private DataLayer dataLayer;


    public void setInventory(PlayerInventory inventory) {
        dataLayer.inventory = inventory;
    }

    public void addItem(Item item, int amount) {
        dataLayer.inventory.addItem(item, amount);
    }

    public void selectNextItem() {
        dataLayer.inventory.selectedTileIndex++;
        if (dataLayer.inventory.selectedTileIndex >= AppConstants.TILES_IN_SELECTOR) {
            dataLayer.inventory.selectedTileIndex = 0;
        }
    }

    public void selectPreviousItem() {
        dataLayer.inventory.selectedTileIndex--;
        if (dataLayer.inventory.selectedTileIndex <= 0) {
            dataLayer.inventory.selectedTileIndex = AppConstants.TILES_IN_SELECTOR - 1;
        }
    }

    public void removeItem(Item item, int amount) {
        try {
            dataLayer.inventory.removeItem(item, amount);
        } catch (CanNotRemoveInventoryItem e) {
            throw new RuntimeException(e);
        }
    }
}
