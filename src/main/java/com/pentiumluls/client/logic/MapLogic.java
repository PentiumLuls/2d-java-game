package com.pentiumluls.client.logic;

import com.pentiumluls.client.DataLayer;
import com.pentiumluls.common.data.Pair;
import com.pentiumluls.common.data.object.Object;
import lombok.AllArgsConstructor;

import java.util.HashMap;

@AllArgsConstructor
public class MapLogic {
    private DataLayer dataLayer;

    public void removeObject(int x, int y) {
        dataLayer.map.objects.remove(new Pair<>(x, y));
    }

    public void placeObject(Object object, int worldX, int worldY) {
        dataLayer.map.objects.put(new Pair<>(worldX, worldY), object);
    }

    public void updateObjects(HashMap<Pair<Integer, Integer>, Object> changedObjects) {
        dataLayer.map.objects.putAll(changedObjects);
    }
}
