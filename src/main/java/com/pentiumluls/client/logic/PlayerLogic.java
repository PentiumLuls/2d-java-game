package com.pentiumluls.client.logic;

import com.pentiumluls.client.AppConstants;
import com.pentiumluls.client.DataLayer;
import com.pentiumluls.client.EventPool;
import com.pentiumluls.client.io.KeyHandler;
import com.pentiumluls.client.event.logic.MoveEvent;
import com.pentiumluls.common.data.Direction;
import com.pentiumluls.client.data.entity.Player;

import java.awt.*;


public class PlayerLogic {
    KeyHandler keyHandler;
    DataLayer dataLayer;

    public Player player;

    public PlayerLogic(DataLayer dataLayer, KeyHandler keyHandler) {
        this.keyHandler = keyHandler;
        this.dataLayer = dataLayer;
        player = dataLayer.player;

        setDefaultValues();
    }

    /* try to load player from save file, or create with default values */
    public void update() {
        // UPDATE DIRECTION
        if (keyHandler.upPressed) {
            player.direction = Direction.UP;
        }
        if (keyHandler.downPressed) {
            player.direction = Direction.DOWN;
        }
        if (keyHandler.leftPressed) {
            player.direction = Direction.LEFT;
            player.lastHorizontalDirection = Direction.LEFT;
        }
        if (keyHandler.rightPressed) {
            player.direction = Direction.RIGHT;
            player.lastHorizontalDirection = Direction.RIGHT;
        }

        // UPDATE POSITION
        if (player.collisionOn == false) {
            if (keyHandler.upPressed) {
                player.worldY -= (player.speed + player.speedBonus);
            }
            if (keyHandler.downPressed) {
                player.worldY += (player.speed + player.speedBonus);
            }
            if (keyHandler.leftPressed) {
                player.worldX -= (player.speed + player.speedBonus);
            }
            if (keyHandler.rightPressed) {
                player.worldX += (player.speed + player.speedBonus);
            }
        }

        if (keyHandler.upPressed || keyHandler.downPressed || keyHandler.leftPressed || keyHandler.rightPressed) {
            player.isMoving = true;
            EventPool.writeNetworkEvent(new MoveEvent(player.direction));
        } else player.isMoving = false;
    }

    public void save() {
//        playerLoader.save(player);
    }

    public void setDefaultValues() {
        player.collisionBox = new Rectangle(15, 28, 22, 36);
        player.collisionEnabled = true;
        player.spriteChangingSpeed = 5;
        player.speed = 4;
        player.direction = Direction.DOWN;
        player.screenX = (AppConstants.SCREEN_WIDTH / 2) - (AppConstants.TILE_SIZE / 2);
        player.screenY = (AppConstants.SCREEN_HEIGHT / 2) - (AppConstants.TILE_SIZE / 2);

    }
}
