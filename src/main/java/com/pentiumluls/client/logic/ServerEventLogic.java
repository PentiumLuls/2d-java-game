package com.pentiumluls.client.logic;

import com.pentiumluls.client.DataLayer;
import com.pentiumluls.client.data.GameState;
import com.pentiumluls.client.data.entity.Player;
import com.pentiumluls.client.event.Event;
import com.pentiumluls.client.event.network.*;
import com.pentiumluls.common.data.Direction;
import com.pentiumluls.common.util.logger.Logger;
import com.pentiumluls.common.util.logger.LoggerLayer;
import lombok.AllArgsConstructor;

/** Handles events from server */
@AllArgsConstructor
public class ServerEventLogic {
    private DataLayer dataLayer;
    private PlayerLogic playerLogic;
    private MapLogic mapLogic;
    private InventoryLogic inventoryLogic;

    /** NOTE!: if method handles another player, the method should be named "anotherPlayer..."
     * for readability */
    public void handleEvent(Event e) {
        if (e instanceof SuccessfulAuthEvent event) {
            successJoinGame(event);

        } else if (e instanceof PlayerJoinedEvent event) {
            anotherPlayerJoined(event);

        } else if (e instanceof PlayerDisconnectedEvent event) {
            anotherPlayerDisconnected(event);

        } else if (e instanceof PlayerMovedEvent event) {
            playerMoved(event); // handles current and other players

        } else if (e instanceof PlayerPickedObjectEvent event) {
            pickObject(event);

        } else if (e instanceof PlaceItemSuccessEvent event) {
            placeItemOnMap(event);

        } else if (e instanceof NewDayEvent event) {
            newDay(event);
        }
    }

    /** current player successfully joined the game.
     * Sets player, inventory, map, other players
     * Also starts the game logic by setting "GameState.PLAY" */
    private void successJoinGame(SuccessfulAuthEvent event) {
        log("Successfully joined the game");
        dataLayer.player.setValuesFrom(event.player);
        dataLayer.setOnlinePlayers(event.onlinePlayers);
        dataLayer.setMap(event.map);
        dataLayer.setEnemies(event.enemies);
        inventoryLogic.setInventory(event.inventory);

        dataLayer.gameState = GameState.PLAY;
        dataLayer.isAuthRequestInProcess = false;
    }

    /** Another player joined. Adds them to list of online players */
    private void anotherPlayerJoined(PlayerJoinedEvent event) {
        log("player " + event.player.name + " joined");
        Player player = new Player(event.player);
        dataLayer.addOnlinePlayer(player);
    }

    /** Another player disconnected. Removes them from online players list */
    private void anotherPlayerDisconnected(PlayerDisconnectedEvent event) {
        log("player " + dataLayer.getOnlinePlayer(event.playerId).username + " disconnected");
        dataLayer.removePlayer(event.playerId);
    }

    /** Current or another player is moved.
     * Updates position of such player */
    private void playerMoved(PlayerMovedEvent event) {
        if (event.playerId == dataLayer.player.id) {
            // received confirmation of movement from server
            dataLayer.player.direction = event.direction;
            dataLayer.player.worldX = event.worldX;
            dataLayer.player.worldY = event.worldY;
        } else {
            // other players movement
            Player player = dataLayer.getOnlinePlayer(event.playerId);
            player.direction = event.direction;
            player.worldX = event.worldX;
            player.worldY = event.worldY;
            player.isMoving = true;
            if (player.direction == Direction.LEFT) {
                player.lastHorizontalDirection = Direction.LEFT;
            } else if (player.direction == Direction.RIGHT) {
                player.lastHorizontalDirection = Direction.RIGHT;
            }
        }
    }

    /** Current player picked up object.
     * Removes it from map, adds to inventory */
    private void pickObject(PlayerPickedObjectEvent event) {
        log("player picked up X" + event.amount + " of item " + event.item.name);
        mapLogic.removeObject(event.x, event.y);
        inventoryLogic.addItem(event.item, event.amount);
    }

    /** Current or another player placed an item on map.
     * Also remove the item(x1) from inventory if placed by current player */
    private void placeItemOnMap(PlaceItemSuccessEvent event) {
        log("success place item on map");
        if (event.playerId == dataLayer.player.id) {
            inventoryLogic.removeItem(event.item, 1);
        }
        mapLogic.placeObject(event.object, event.worldX, event.worldY);
    }

    /** New day. Update changed objects(during the event) */
    private void newDay(NewDayEvent event) {
        log("new day");
        mapLogic.updateObjects(event.changedObjects);
    }

    private void log(String message) {
        Logger.log(LoggerLayer.LOGIC, message);
    }
}
