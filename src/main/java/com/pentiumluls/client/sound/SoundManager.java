package com.pentiumluls.client.sound;

import javax.sound.sampled.*;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class SoundManager {
    Clip clip;
    List<URL> soundUrl = new ArrayList<>();

    public SoundManager() {
        for (SoundType type : SoundType.values()) {
            soundUrl.add(getClass().getResource(type.filePath));
        }
    }

    public void setFile(SoundType type) {
        try {
            AudioInputStream stream = AudioSystem.getAudioInputStream(soundUrl.get(type.id));
            clip = AudioSystem.getClip();
            clip.open(stream);
        } catch (UnsupportedAudioFileException | LineUnavailableException | IOException e) {
            e.printStackTrace();
        }
    }

    public void play() {
        clip.start();
    }

    public void loop() {
        clip.loop(Clip.LOOP_CONTINUOUSLY);
    }

    public void stop() {
        clip.stop();
    }
}
