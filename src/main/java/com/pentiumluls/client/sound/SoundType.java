package com.pentiumluls.client.sound;

public enum SoundType {
    BACKGROUND(0, "/sound/main_ost.wav"),
    POWERUP(1, "/sound/powerup.wav"),
    UNLOCK(2, "/sound/unlock.wav"),
    ;

    public final String filePath;
    public final int id;

    SoundType(int id, String filePath) {
        this.id = id;
        this.filePath = filePath;
    }
}
