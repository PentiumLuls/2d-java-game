package com.pentiumluls.client.ui;

import com.pentiumluls.client.AppConstants;
import com.pentiumluls.client.DataLayer;
import com.pentiumluls.client.ui.animation.SpriteSheetHelper;
import com.pentiumluls.common.data.enemy.Enemy;
import com.pentiumluls.common.data.enemy.EnemyType;

import java.awt.*;
import java.awt.image.BufferedImage;

public class EnemyUI {
    private DataLayer dataLayer;
    private SpriteSheetHelper sheetHelper;
    private FontHelper fontHelper;

    // TODO: remove hardcode
    BufferedImage dummySprite;

    public EnemyUI(DataLayer dataLayer, SpriteSheetHelper sheetHelper, FontHelper fontHelper) {
        this.dataLayer = dataLayer;
        this.sheetHelper = sheetHelper;
        this.fontHelper = fontHelper;

        // TODO: extract to another class
        loadSprites();
    }

    public void draw(Graphics2D g2) {
        for (Enemy enemy : dataLayer.getEnemies()) {
            drawEnemy(g2, enemy);
        }
    }

    private void drawEnemy(Graphics2D g2, Enemy enemy) {
        // CALCULATE COORDINATES RELATIVE TO CLIENT SCREEN
        int screenX = enemy.worldX - dataLayer.player.worldX + dataLayer.player.screenX;
        int screenY = enemy.worldY - dataLayer.player.worldY + dataLayer.player.screenY;

        // DRAW ENENMY SPRITE
        BufferedImage image;
        g2.drawImage(dummySprite,
                screenX - AppConstants.TILE_SIZE / Enemy.spriteScale,
                screenY - AppConstants.TILE_SIZE / Enemy.spriteScale,
                AppConstants.TILE_SIZE * Enemy.spriteScale,
                AppConstants.TILE_SIZE * Enemy.spriteScale, null);
    }

    private void loadSprites() {
        BufferedImage sheet = sheetHelper.getSpriteSheet("/enemy/" + EnemyType.DUMMY.getImageName());
        dummySprite = sheetHelper.getSprite(sheet, 0, 0);
    }
}
