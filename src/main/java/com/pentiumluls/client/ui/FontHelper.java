package com.pentiumluls.client.ui;

import java.awt.*;
import java.io.IOException;
import java.io.InputStream;

public class FontHelper {
    public Font commonFont;
    public Font playerName;
    public Font itemAmount;

    public FontHelper() {
        try {
            GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
            InputStream fileStream = getClass().getResourceAsStream("/font/yoster.ttf");
            commonFont = Font.createFont(Font.TRUETYPE_FONT, fileStream);
            ge.registerFont(commonFont);

            commonFont = commonFont.deriveFont(Font.PLAIN, 20);
            playerName = commonFont.deriveFont(Font.PLAIN, 12);
            itemAmount = commonFont.deriveFont(Font.PLAIN, 14);

        } catch (IOException | FontFormatException e) {
            commonFont = new Font("Arial", Font.PLAIN, 20);
            e.printStackTrace();
        }
    }
}
