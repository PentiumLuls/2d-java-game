package com.pentiumluls.client.ui;

import com.pentiumluls.client.io.MouseHandler;
import com.pentiumluls.client.io.MouseWheelHandler;
import com.pentiumluls.client.ui.animation.SpriteSheetHelper;
import com.pentiumluls.client.AppConstants;
import com.pentiumluls.client.DataLayer;
import com.pentiumluls.client.io.KeyHandler;
import com.pentiumluls.client.data.GameState;
import com.pentiumluls.client.ui.map.MapUI;
import com.pentiumluls.common.util.Benchmark;
import com.pentiumluls.client.sound.SoundManager;
import com.pentiumluls.client.sound.SoundType;

import javax.swing.*;
import java.awt.*;

// TODO: extract JPanel class to another file
public class GamePanel extends JPanel implements Runnable {
    DataLayer dataLayer;

    Thread gameThread;
    Benchmark benchmark = new Benchmark();

    public SpriteSheetHelper spriteSheetHelper = new SpriteSheetHelper();
    public SoundManager soundManager = new SoundManager();

    public FontHelper fontHelper = new FontHelper();
    public LoginUI loginUI;
    public PlayerUI playerUI;
    public OtherPlayersUI otherPlayersUI;
    public MapUI mapUI;
    public InventoryUI inventoryUI;
    public EnemyUI enemyUI;


    public GamePanel(DataLayer dataLayer) {
        this.dataLayer = dataLayer;
        playerUI = new PlayerUI(dataLayer, spriteSheetHelper, fontHelper);
        otherPlayersUI = new OtherPlayersUI(dataLayer, spriteSheetHelper, fontHelper);
        loginUI = new LoginUI(fontHelper, dataLayer, this);
        mapUI = new MapUI(dataLayer, this);
        inventoryUI = new InventoryUI(dataLayer, fontHelper, this);
        enemyUI = new EnemyUI(dataLayer, spriteSheetHelper, fontHelper);

        this.setPreferredSize(new Dimension(AppConstants.SCREEN_WIDTH, AppConstants.SCREEN_HEIGHT));
        this.setBackground(Color.BLACK);
        this.setDoubleBuffered(true); // increase rendering performance
        this.addKeyListener(KeyHandler.instance());
        this.addMouseWheelListener(MouseWheelHandler.instance());
        this.addMouseListener(MouseHandler.instance());
        this.setFocusable(true);
        playBackgroundMusic();
    }

    public void startGameThread() {
        gameThread = new Thread(this);
        gameThread.start();
    }

    // Game loop
    @Override
    public void run() {
        double drawInterval = 1000000000 / AppConstants.FPS;
        double delta = 0;
        long lastTime = System.nanoTime();
        long currentTime;
        long timer = 0;
        int drawCount = 0;

        while (gameThread != null) {
            currentTime = System.nanoTime();
            delta += (currentTime - lastTime) / drawInterval;
            timer += (currentTime - lastTime);
            lastTime = currentTime;

            if (delta >= 1) {
                // ACTUAL GAME LOOP, FRAMED IN PREDEFINED FPS
                repaint();

                delta--;
                drawCount++;
            }
            if (timer >= 1000000000) {
                System.out.println("FPS: " + drawCount);
                drawCount = 0;
                timer = 0;
            }
        }
    }

    // called on repaint()
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
//        benchmark.start("GamePanel.paintComponent");
        Graphics2D g2 = (Graphics2D) g;

        if (dataLayer.gameState == GameState.PLAY) {
            mapUI.draw(g2);
            otherPlayersUI.draw(g2);
            playerUI.draw(g2);
            inventoryUI.draw(g2);
            enemyUI.draw(g2);

        } else if (dataLayer.gameState == GameState.LOGIN) {
            loginUI.draw(g2);
        }
//        benchmark.stop();
        g2.dispose();
    }

    /** play endlessly */
    public void playBackgroundMusic() {
        soundManager.setFile(SoundType.BACKGROUND);
        soundManager.play();
        soundManager.loop();
    }

    /** play once */
    public void playSound(SoundType type) {
        soundManager.setFile(type);
        soundManager.play();
    }

    public void stopMusic() {
        soundManager.stop();
    }

    /** save all progress */
    public void saveKeyPressed() {
//        mapManager.saveMapTiles("test_map");
//        playerManager.save();
    }
}
