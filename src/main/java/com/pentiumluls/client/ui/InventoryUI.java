package com.pentiumluls.client.ui;

import com.pentiumluls.client.AppConstants;
import com.pentiumluls.client.DataLayer;
import com.pentiumluls.common.data.PlayerInventory;
import com.pentiumluls.common.data.item.InventorySlot;
import com.pentiumluls.common.io.ItemImageLoader;

import java.awt.*;
import java.awt.image.BufferedImage;

public class InventoryUI {
    private final DataLayer dataLayer;
    private final FontHelper fontHelper;
    private final GamePanel gamePanel;
    private final ItemImageLoader itemImageLoader = ItemImageLoader.instance();

    public InventoryUI(DataLayer dataLayer, FontHelper fontHelper, GamePanel gamePanel) {
        this.dataLayer = dataLayer;
        this.fontHelper = fontHelper;
        this.gamePanel = gamePanel;
    }

    public void draw(Graphics2D g2) {
        PlayerInventory inventory = dataLayer.inventory;
        int y = gamePanel.getHeight() - AppConstants.SELECTOR_HEIGHT;
        // background
        g2.setColor(Color.GRAY);
        g2.fillRect(0, y,
                AppConstants.SCREEN_WIDTH, AppConstants.SELECTOR_HEIGHT);

        int startIndex = inventory.selectorTab * AppConstants.TILES_IN_SELECTOR;
        g2.setStroke(new BasicStroke(3.0f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER, 10.0f));
        int maxIndex = inventory.size();

        for (int row = 0; row < AppConstants.TILES_IN_SELECTOR; row++) {
            int index = startIndex + row;

            // hotbar grid
            int screenX = drawGridCell(g2, y, row);

            if (index >= maxIndex)
                continue;
            drawItem(g2, inventory, screenX, y, index);
        }

        // draw outline on selected tile
        g2.setColor(Color.RED);
        g2.drawRect(inventory.selectedTileIndex * (AppConstants.TILE_SIZE + AppConstants.SELECTOR_BORDER), y,
                AppConstants.TILE_SIZE + AppConstants.SELECTOR_BORDER, AppConstants.TILE_SIZE + AppConstants.SELECTOR_BORDER);

    }

    private static int drawGridCell(Graphics2D g2, int y, int row) {
        int screenX = row * (AppConstants.TILE_SIZE + AppConstants.SELECTOR_BORDER);
        g2.setColor(Color.BLACK);
        g2.drawRect(screenX, y,
                AppConstants.TILE_SIZE + AppConstants.SELECTOR_BORDER, AppConstants.TILE_SIZE + AppConstants.SELECTOR_BORDER);
        return screenX;
    }

    private void drawItem(Graphics2D g2, PlayerInventory inventory, int x, int y, int index) {
        InventorySlot item = inventory.get(index);
        BufferedImage image;
        image = itemImageLoader.getImageForInventory(item.item);
        g2.drawImage(image,
                x + AppConstants.SELECTOR_BORDER, y + AppConstants.SELECTOR_BORDER, null);

        // Item amount
        if (item.amount > 1) {
            g2.setFont(fontHelper.itemAmount);
            g2.setColor(Color.WHITE);
            String text = String.valueOf(item.amount);
            var textBounds = g2.getFontMetrics().getStringBounds(text, g2);
            int textX = x + AppConstants.TILE_SIZE - ((int) textBounds.getWidth());
            int textY = y + AppConstants.TILE_SIZE;
            g2.drawString(text, textX, textY);
        }
    }
}
