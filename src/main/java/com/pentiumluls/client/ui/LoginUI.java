package com.pentiumluls.client.ui;

import com.pentiumluls.client.AppConstants;
import com.pentiumluls.client.DataLayer;
import com.pentiumluls.client.ui.components.TextField;
import com.pentiumluls.common.logic.Command;
import com.pentiumluls.client.command.SubmitInputCommand;

import java.awt.*;

public class LoginUI {
    private FontHelper fontHelper;
    private DataLayer dataLayer;
    private GamePanel gamePanel;

    private TextField textField;

    public LoginUI(FontHelper fontHelper, DataLayer dataLayer, GamePanel gamePanel) {
        this.fontHelper = fontHelper;
        this.dataLayer = dataLayer;
        this.gamePanel = gamePanel;

        Command command = new SubmitInputCommand(this);
        Rectangle inputBox = new Rectangle(AppConstants.SCREEN_WIDTH / 2 - 125,
                AppConstants.SCREEN_HEIGHT / 3 + 30,
                250, 35);
        textField = new TextField(fontHelper.commonFont, inputBox, 16, command);
        textField.onFocus();
        gamePanel.addKeyListener(textField);

    }

    public void draw(Graphics2D g2) {
        g2.setFont(fontHelper.commonFont);
        g2.setColor(Color.WHITE);
        drawPauseLabel(g2);

        textField.draw(g2);
    }

    private void drawPauseLabel(Graphics2D g2) {
        String text = "Enter your mighty name:";
        int fontWidth = (int) g2.getFontMetrics().getStringBounds(text, g2).getWidth();
        int x = (AppConstants.SCREEN_WIDTH / 2) - (fontWidth / 2);
        int y = AppConstants.SCREEN_HEIGHT / 3;
        g2.drawString(text, x, y);
    }

    public void submitLogin() {
        dataLayer.player.username = textField.getInput();
    }
}
