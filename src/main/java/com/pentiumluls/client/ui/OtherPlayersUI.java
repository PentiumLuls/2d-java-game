package com.pentiumluls.client.ui;

import com.pentiumluls.client.ui.animation.SpriteSheetHelper;
import com.pentiumluls.client.AppConstants;
import com.pentiumluls.client.DataLayer;
import com.pentiumluls.common.data.Direction;
import com.pentiumluls.client.data.entity.Player;
import org.jetbrains.annotations.NotNull;

import java.awt.*;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

public class OtherPlayersUI {
    SpriteSheetHelper sheetHelper;
    DataLayer dataLayer;
    FontHelper fontHelper;

    List<BufferedImage> standSprites;
    List<BufferedImage> leftSprites;
    List<BufferedImage> rightSprites;

    public OtherPlayersUI(DataLayer dataLayer, SpriteSheetHelper spriteSheetHelper, FontHelper fontHelper) {
        this.sheetHelper = spriteSheetHelper;
        this.dataLayer = dataLayer;
        this.fontHelper = fontHelper;

        loadPlayerImages();
    }

    public void draw(Graphics2D g2) {
        for (Player player : dataLayer.getOnlinePlayers()) {
            drawPlayer(g2, player);
        }
    }

    private void drawPlayer(Graphics2D g2, Player player) {
        List<BufferedImage> currentSpriteList = getCurrentSpriteList(player);

        // UPDATE ANIMATION FRAMES
        player.spriteCounter++;
        if (player.spriteCounter > player.spriteChangingSpeed) { // delay sprite changing
            player.spriteIndex++;
            if (player.spriteIndex >= currentSpriteList.size()) {
                player.spriteIndex = 0;
            }
            player.spriteCounter = 0;
        }

        // CALCULATE COORDINATES RELATIVE TO CLIENT SCREEN
        int screenX = player.worldX - dataLayer.player.worldX + dataLayer.player.screenX;
        int screenY = player.worldY - dataLayer.player.worldY + dataLayer.player.screenY;

        // DRAW PLAYER SPRITE
        BufferedImage image;
        image = currentSpriteList.get(player.spriteIndex);
        if (player.lastHorizontalDirection == Direction.LEFT) { // flip sprite to left side
            g2.drawImage(image,
                    screenX + (AppConstants.TILE_SIZE * player.spriteScale) - AppConstants.TILE_SIZE / player.spriteScale,
                    screenY - AppConstants.TILE_SIZE / player.spriteScale,
                    -(AppConstants.TILE_SIZE * player.spriteScale),
                    (AppConstants.TILE_SIZE * player.spriteScale), null);
        } else {
            g2.drawImage(image,
                    screenX - AppConstants.TILE_SIZE / player.spriteScale,
                    screenY - AppConstants.TILE_SIZE / player.spriteScale,
                    AppConstants.TILE_SIZE * player.spriteScale,
                    AppConstants.TILE_SIZE * player.spriteScale, null);
        }

        // DRAW NAME
        g2.setFont(fontHelper.playerName);
        g2.setColor(Color.BLACK);
        Rectangle2D stringBounds = g2.getFontMetrics().getStringBounds(player.username, g2);
        int fontWidth = (int) stringBounds.getWidth();
        int fontHeight = (int) stringBounds.getHeight();
        int x = (screenX + AppConstants.TILE_SIZE / 2) - fontWidth / 2;
        int y = screenY + AppConstants.TILE_SIZE / 3;

        // name frame
        Composite composite = g2.getComposite();
        g2.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.3f)); // 0.5 transparency
        g2.fillRect(x - 10, (y - fontHeight), (20 + fontWidth), (6 + fontHeight));
        g2.setComposite(composite);

        g2.drawString(player.username, x, y);

//        drawCollisionBox(g2, player, screenX, screenY);
    }

    private void drawCollisionBox(Graphics2D g2, Player player, int screenX, int screenY) {
        g2.drawRoundRect(screenX + player.collisionBox.x,
                screenY + player.collisionBox.y,
                player.collisionBox.width, player.collisionBox.height,
                1, 1);
    }

    public void loadPlayerImages() {
        BufferedImage sheet = sheetHelper.getSpriteSheet("/player/player.png");

        standSprites = new ArrayList<>();
        standSprites.add(sheetHelper.getSprite(sheet, 0, 0));
        standSprites.add(sheetHelper.getSprite(sheet, 1, 0));
        standSprites.add(sheetHelper.getSprite(sheet, 2, 0));
        standSprites.add(sheetHelper.getSprite(sheet, 3, 0));
        standSprites.add(sheetHelper.getSprite(sheet, 4, 0));
        standSprites.add(sheetHelper.getSprite(sheet, 5, 0));

        leftSprites = new ArrayList<>();
        leftSprites.add(sheetHelper.getSprite(sheet, 0, 1));
        leftSprites.add(sheetHelper.getSprite(sheet, 1, 1));
        leftSprites.add(sheetHelper.getSprite(sheet, 2, 1));
        leftSprites.add(sheetHelper.getSprite(sheet, 3, 1));
        leftSprites.add(sheetHelper.getSprite(sheet, 4, 1));
        leftSprites.add(sheetHelper.getSprite(sheet, 5, 1));

        rightSprites = new ArrayList<>();
        rightSprites.add(sheetHelper.getSprite(sheet, 0, 1));
        rightSprites.add(sheetHelper.getSprite(sheet, 1, 1));
        rightSprites.add(sheetHelper.getSprite(sheet, 2, 1));
        rightSprites.add(sheetHelper.getSprite(sheet, 3, 1));
        rightSprites.add(sheetHelper.getSprite(sheet, 4, 1));
        rightSprites.add(sheetHelper.getSprite(sheet, 5, 1));
    }

    @NotNull
    private List<BufferedImage> getCurrentSpriteList(Player player) {
        if (!player.isMoving) return standSprites;

        switch (player.direction) {
            case UP, DOWN -> {
                if (player.lastHorizontalDirection == Direction.LEFT) return leftSprites;
                return rightSprites;
            }
            case LEFT -> { return leftSprites; }
            case RIGHT -> { return rightSprites; }
        }
        return null; // Never triggered
    }

}
