package com.pentiumluls.client.ui;

import com.pentiumluls.client.AppConstants;
import com.pentiumluls.client.DataLayer;
import com.pentiumluls.client.data.entity.Player;
import com.pentiumluls.client.ui.animation.SpriteSheetHelper;
import com.pentiumluls.common.data.Direction;
import com.pentiumluls.common.data.item.Item;
import com.pentiumluls.common.io.ItemImageLoader;
import org.jetbrains.annotations.NotNull;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

public class PlayerUI {
    SpriteSheetHelper sheetHelper;
    DataLayer dataLayer;
    FontHelper fontHelper;
    private final ItemImageLoader itemImageLoader = ItemImageLoader.instance();


    public PlayerUI(DataLayer dataLayer, SpriteSheetHelper spriteSheetHelper, FontHelper fontHelper) {
        this.sheetHelper = spriteSheetHelper;
        this.dataLayer = dataLayer;
        this.fontHelper = fontHelper;

        loadPlayerImages();
    }

    public void draw(Graphics2D g2) {
        Player player = dataLayer.player;
        List<BufferedImage> currentSpriteList = getCurrentSpriteList(player);

        // UPDATE ANIMATION FRAMES
        player.spriteCounter++;
        if (player.spriteCounter > player.spriteChangingSpeed) { // delay sprite changing
            player.spriteIndex++;
            if (player.spriteIndex >= currentSpriteList.size()) {
                player.spriteIndex = 0;
            }
            player.spriteCounter = 0;
        }

        dataLayer.inventory.getCurrentItem()
                        .ifPresent(item -> drawHeldItem(g2, player, item));
        drawPlayerSprite(g2, player, currentSpriteList);
        drawPlayerName(g2, player);
//        drawCollisionBox(g2, player);
    }

    private void drawPlayerSprite(Graphics2D g2, Player player, List<BufferedImage> currentSpriteList) {
        BufferedImage image;
        image = currentSpriteList.get(player.spriteIndex);
        if (player.lastHorizontalDirection == Direction.LEFT) { // flip sprite to left side
            g2.drawImage(image,
                    player.screenX + (AppConstants.TILE_SIZE * player.spriteScale) - AppConstants.TILE_SIZE / player.spriteScale,
                    player.screenY - AppConstants.TILE_SIZE / player.spriteScale,
                    -(AppConstants.TILE_SIZE * player.spriteScale),
                    (AppConstants.TILE_SIZE * player.spriteScale), null);
        } else {
            g2.drawImage(image,
                    player.screenX - AppConstants.TILE_SIZE / player.spriteScale,
                    player.screenY - AppConstants.TILE_SIZE / player.spriteScale,
                    AppConstants.TILE_SIZE * player.spriteScale,
                    AppConstants.TILE_SIZE * player.spriteScale, null);
        }
    }

    /* based on player's direction */
    private void drawHeldItem(Graphics2D g2, Player player, Item item) {
        int x, rotationAngle;
        int y = player.screenY + 25;
        if (player.lastHorizontalDirection == Direction.LEFT) {
            x = player.screenX - 15;
            rotationAngle = -45;
        } else {
            x = player.screenX + 5 + AppConstants.TILE_SIZE / player.spriteScale;
            rotationAngle = 45;
        }

        BufferedImage image = itemImageLoader.getImageForHolding(item);

        // rotate image
        double rotationRequired = Math.toRadians(rotationAngle);
        AffineTransform tx = AffineTransform.getRotateInstance(rotationRequired,
                image.getWidth() / 2,
                image.getHeight() / 2);
        AffineTransformOp op = new AffineTransformOp(tx, AffineTransformOp.TYPE_BILINEAR);

        // Drawing the rotated image at the required drawing locations
        g2.drawImage(op.filter(image, null), x, y, null);
    }

    private void drawPlayerName(Graphics2D g2, Player player) {
        g2.setFont(fontHelper.playerName);
        g2.setColor(Color.BLACK);
        Rectangle2D stringBounds = g2.getFontMetrics().getStringBounds(player.username, g2);
        int fontWidth = (int) stringBounds.getWidth();
        int fontHeight = (int) stringBounds.getHeight();
        int x = (player.screenX + AppConstants.TILE_SIZE / 2) - fontWidth / 2;
        int y = player.screenY + AppConstants.TILE_SIZE / 3;

        // name frame
        Composite composite = g2.getComposite();
        g2.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.3f)); // 0.5 transparency
        g2.fillRect(x - 10, (y - fontHeight), (20 + fontWidth), (6 + fontHeight));
        g2.setComposite(composite);

        g2.drawString(player.username, x, y);
    }

    private void drawCollisionBox(Graphics2D g2, Player player) {
        g2.drawRoundRect(player.screenX + player.collisionBox.x,
                player.screenY + player.collisionBox.y,
                player.collisionBox.width, player.collisionBox.height,
                1, 1);
    }

    public void loadPlayerImages() {
        Player player = dataLayer.player;
        BufferedImage sheet = sheetHelper.getSpriteSheet("/player/player.png");

        player.standSprites = new ArrayList<>();
        player.standSprites.add(sheetHelper.getSprite(sheet, 0, 0));
        player.standSprites.add(sheetHelper.getSprite(sheet, 1, 0));
        player.standSprites.add(sheetHelper.getSprite(sheet, 2, 0));
        player.standSprites.add(sheetHelper.getSprite(sheet, 3, 0));
        player.standSprites.add(sheetHelper.getSprite(sheet, 4, 0));
        player.standSprites.add(sheetHelper.getSprite(sheet, 5, 0));

        player.leftSprites = new ArrayList<>();
        player.leftSprites.add(sheetHelper.getSprite(sheet, 0, 1));
        player.leftSprites.add(sheetHelper.getSprite(sheet, 1, 1));
        player.leftSprites.add(sheetHelper.getSprite(sheet, 2, 1));
        player.leftSprites.add(sheetHelper.getSprite(sheet, 3, 1));
        player.leftSprites.add(sheetHelper.getSprite(sheet, 4, 1));
        player.leftSprites.add(sheetHelper.getSprite(sheet, 5, 1));

        player.rightSprites = new ArrayList<>();
        player.rightSprites.add(sheetHelper.getSprite(sheet, 0, 1));
        player.rightSprites.add(sheetHelper.getSprite(sheet, 1, 1));
        player.rightSprites.add(sheetHelper.getSprite(sheet, 2, 1));
        player.rightSprites.add(sheetHelper.getSprite(sheet, 3, 1));
        player.rightSprites.add(sheetHelper.getSprite(sheet, 4, 1));
        player.rightSprites.add(sheetHelper.getSprite(sheet, 5, 1));
    }

    @NotNull
    private List<BufferedImage> getCurrentSpriteList(Player player) {
        if (!player.isMoving) return player.standSprites;

        switch (player.direction) {
            case UP, DOWN -> {
                if (player.lastHorizontalDirection == Direction.LEFT) return player.leftSprites;
                return player.rightSprites;
            }
            case LEFT -> { return player.leftSprites; }
            case RIGHT -> { return player.rightSprites; }
        }
        return null; // Never triggered
    }

}
