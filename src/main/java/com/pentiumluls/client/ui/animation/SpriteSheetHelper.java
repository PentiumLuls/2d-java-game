package com.pentiumluls.client.ui.animation;

import com.pentiumluls.client.AppConstants;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;

public class SpriteSheetHelper {

    /** returns sprite from given spriteSheet */
    public BufferedImage getSprite(BufferedImage spriteSheet, int xGrid, int yGrid) {
        return spriteSheet.getSubimage(xGrid * AppConstants.TILE_SIZE, yGrid * AppConstants.TILE_SIZE,
                AppConstants.TILE_SIZE, AppConstants.TILE_SIZE);
    }

    /** @param filePath - relative path to image from src/main/resources
     *   ex: filePath="/player/player.png" will load src/main/resources/player/player.png */
    public BufferedImage getSpriteSheet(String filePath) {

        BufferedImage sprite = null;

        try {
            sprite = ImageIO.read(getClass().getResourceAsStream(filePath));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return sprite;
    }
}
