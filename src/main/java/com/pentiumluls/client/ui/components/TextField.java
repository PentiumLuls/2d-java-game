package com.pentiumluls.client.ui.components;

import com.pentiumluls.common.logic.Command;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class TextField implements KeyListener {
    private static final String CURSOR = "_";
    private StringBuffer buffer;
    private String showableText;
    private int index;
    private Font font;
    private int maxLength;

    private Rectangle rectangle;
    private Point textPoint;

    private int padding;

    private int cursorDelay;
    private int countDelay;
    private boolean cursor;
    private int cursorLength;

    private boolean focus;
    private Command submitCommand;

    public TextField(Font font, Rectangle rectangle, int maxLength, Command submitCommand) {
        this.submitCommand = submitCommand;
        /* Get default parameters */
        this.padding = 20;
        this.cursorDelay = 10;

        /* Set attributes with constructor parameters */
        this.font = font;
        this.rectangle = rectangle;

        /* Set text point */
        int x = this.rectangle.x + this.padding;
        int y = this.rectangle.y;
        this.textPoint = new Point(x, y);

        /* Create variables to control buffer */
        this.buffer = new StringBuffer();
        this.showableText = "";
        this.maxLength = maxLength;
        this.clearIndex();

        /* Create variables to control cursor */
        this.countDelay = 0;
        this.cursor = false;
        this.cursorLength = 5;

        /* Starts without focus */
        this.focus = false;
    }

    private void clearTextField() {
        this.focus = false;
        this.buffer = new StringBuffer();
        this.showableText = "";
        this.clearIndex();
    }

//    private int getAdjustedTextWidth(String text) {
//        return font.getSpacing(text).width + cursorLength + (2 * padding);
//    }

    public synchronized void onFocus() {
        focus = true;
    }

    /**
     * return entered string
     */
    public String getInput() {
        return this.buffer.toString();
    }

    private void clearIndex() {
        this.index = 0;
    }

    private int getIndex() {
        return index;
    }

    private void increaseIndex() {
        index++;
    }

    /**
     * delete last character
     */
    public void removeLastChar() {
        if (buffer.length() > 0) {
            clearIndex();
            buffer.deleteCharAt(buffer.length() - 1);

            /* Get showable text with index */
            showableText = buffer.substring(getIndex(), buffer.length());

            /* Adjust showable text if necessary */
//            while ( getAdjustedTextWidth(showableText) > rectangle.width ) {
//                increaseIndex();
//                showableText = buffer.substring( getIndex() , buffer.length() );
//            }
        }
    }

    /**
     * delete character to the end of input
     */
    public void addChar(char character) {

        /* Validate size */
        if (buffer.length() >= maxLength) {
            return;
        }

        /* Add char in buffer */
        buffer.append(character);

        /* Get showable text with index */
        showableText = buffer.substring(getIndex(), buffer.length());

        /* Adjust showable text if necessary */
//        while ( getAdjustedTextWidth(showableText) > rectangle.width ) {
//            increaseIndex();
//            showableText = buffer.substring( getIndex() , buffer.length() );
//        }
    }

    public void draw(Graphics2D g2) {

        if (focus) {
            /* border */
            g2.setColor(Color.white);
            g2.drawRect(rectangle.x, rectangle.y, rectangle.width, rectangle.height);

            /* cursor */
            countDelay++;
            countDelay = countDelay % cursorDelay;
            if (countDelay == 0) {
                cursor = !cursor;
            }

            /* Set text to draw */
            String text = showableText + ((cursor) ? CURSOR : "");

            /* text */
            g2.setFont(font);
            int fontHeight = (int) g2.getFontMetrics().getStringBounds(text, g2).getHeight();
            g2.drawString(text, textPoint.x, textPoint.y + fontHeight);
        }
    }

    @Override
    public void keyTyped(KeyEvent keyEvent) {
    }

    @Override
    public void keyPressed(KeyEvent keyEvent) {
        if (keyEvent.getKeyCode() == KeyEvent.VK_BACK_SPACE) {
            removeLastChar();
        } else if (keyEvent.getKeyCode() == KeyEvent.VK_ENTER) {
            submitCommand.execute();
        } else if (keyEvent.getKeyCode() >= 45 && keyEvent.getKeyCode() <= 90) {
            addChar(keyEvent.getKeyChar());
        }
    }

    @Override
    public void keyReleased(KeyEvent keyEvent) {
    }
}
