package com.pentiumluls.client.ui.map;

import com.pentiumluls.client.AppConstants;
import com.pentiumluls.client.DataLayer;
import com.pentiumluls.client.data.entity.Player;
import com.pentiumluls.client.ui.GamePanel;
import com.pentiumluls.common.data.Pair;
import com.pentiumluls.common.data.map.LayeredTile;
import com.pentiumluls.common.data.map.Tile;
import com.pentiumluls.common.data.object.Object;
import com.pentiumluls.common.io.ObjectImageLoader;
import com.pentiumluls.common.io.TileImageLoader;

import java.awt.*;
import java.util.Map;

/** Controls every tile in map */
public class MapUI {
    private DataLayer dataLayer;
    private GamePanel gamePanel;

    private TileImageLoader tileImageLoader = new TileImageLoader();
    private ObjectImageLoader objectImageLoader = new ObjectImageLoader();

    public MapUI(DataLayer dataLayer, GamePanel gamePanel) {
        this.dataLayer = dataLayer;
        this.gamePanel = gamePanel;
    }

    public void draw(Graphics2D g2) {
        Player player = dataLayer.player;

        drawBasicTiles(g2, player);
        drawLayeredTiles(g2, player);
        drawObjects(g2, player);
        if (dataLayer.inventory.isPlaceAble()) {
            drawHover(g2);
        }
    }

    private void drawBasicTiles(Graphics2D g2, Player player) {
        for (int column = 0; column < dataLayer.mapWidth; column++) {
            for (int row = 0; row < dataLayer.mapHeight; row++) {
                Tile tile = dataLayer.map.tiles[column][row];
                int worldX = column * AppConstants.TILE_SIZE;
                int worldY = row * AppConstants.TILE_SIZE;
                int screenX = worldX - player.worldX + player.screenX;
                int screenY = worldY - player.worldY + player.screenY;

                // draw if only displayed on screen
                if (screenX > (-AppConstants.TILE_SIZE) && screenX < AppConstants.SCREEN_WIDTH
                        && screenY > (-AppConstants.TILE_SIZE) && screenY < AppConstants.SCREEN_HEIGHT) {
                    g2.drawImage(tileImageLoader.getImage(tile),
                            screenX, screenY, null);

//                    drawTileCoordinate(g2, column, row, screenX, screenY);
//                    drawGrid(g2, screenX, screenY);
                }
            }
        }
    }

    private void drawLayeredTiles(Graphics2D g2, Player player) {
        for (LayeredTile tile : dataLayer.map.layeredTiles.values()) {
            int worldX = tile.x * AppConstants.TILE_SIZE;
            int worldY = tile.y * AppConstants.TILE_SIZE;
            int screenX = worldX - player.worldX + player.screenX;
            int screenY = worldY - player.worldY + player.screenY;

            // draw if only displayed on screen
            if (screenX > (-AppConstants.TILE_SIZE) && screenX < AppConstants.SCREEN_WIDTH
                    && screenY > (-AppConstants.TILE_SIZE) && screenY < AppConstants.SCREEN_HEIGHT) {
                g2.drawImage(tileImageLoader.getImage(tile),
                        screenX, screenY, null);

//                drawTileCoordinate(g2, tile.x, tile.y, screenX, screenY);
//                    drawGrid(g2, screenX, screenY);
            }

        }
    }

    private void drawObjects(Graphics2D g2, Player player) {
        for (Map.Entry<Pair<Integer, Integer>, Object> entry : dataLayer.map.objects.entrySet()) {
            int worldX = entry.getKey().getFirst() * AppConstants.TILE_SIZE;
            int worldY = entry.getKey().getSecond() * AppConstants.TILE_SIZE;
            int screenX = worldX - player.worldX + player.screenX;
            int screenY = worldY - player.worldY + player.screenY;

            // draw if only displayed on screen
            if (screenX > (-AppConstants.TILE_SIZE) && screenX < AppConstants.SCREEN_WIDTH
                    && screenY > (-AppConstants.TILE_SIZE) && screenY < AppConstants.SCREEN_HEIGHT) {
                g2.drawImage(objectImageLoader.getImage(entry.getValue()),
                        screenX, screenY, null);
            }
        }
    }

    /** draw transparent rectangle over hover tile */
    private void drawHover(Graphics2D g2) {
        Point mousePosition = gamePanel.getMousePosition();
        if (mousePosition == null)
            return;
        int mouseX = mousePosition.x;
        int mouseY = mousePosition.y;

        var cords = getHoveredTileCoordinates(mouseX, mouseY);
        if (cords == null)
            return;
        int column = cords.getFirst();
        int row = cords.getSecond();
        dataLayer.hoveredTileX = column;
        dataLayer.hoveredTileY = row;

        // tile position on screen
        int tileX = column * AppConstants.TILE_SIZE - dataLayer.player.worldX + dataLayer.player.screenX;
        int tileY = row * AppConstants.TILE_SIZE - dataLayer.player.worldY + dataLayer.player.screenY;

        Rectangle rect = new Rectangle(tileX, tileY, AppConstants.TILE_SIZE, AppConstants.TILE_SIZE);
        Composite composite = g2.getComposite();
        g2.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.5f));
        g2.fill(rect);
        g2.setComposite(composite);
    }

    private void drawTileCoordinate(Graphics2D g2, int column, int row, int screenX, int screenY) {
        g2.drawString("" + column + ":" + row, screenX, screenY);
    }

    private void drawGrid(Graphics2D g2, int screenX, int screenY) {
        g2.drawRoundRect(screenX, screenY, screenX + AppConstants.TILE_SIZE, screenY + AppConstants.TILE_SIZE, 1, 1);
    }

    /** return column and row, wrapped in Pair of tile */
    private Pair<Integer, Integer> getHoveredTileCoordinates(int mouseX, int mouseY) {
        // tile coordinate on grid. Could be 0
        int column = (mouseX + dataLayer.player.worldX - dataLayer.player.screenX) / AppConstants.TILE_SIZE;
        int row = (mouseY + dataLayer.player.worldY - dataLayer.player.screenY) / AppConstants.TILE_SIZE;

        return new Pair<>(column, row);
    }
}
