package com.pentiumluls.common.data;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public enum Direction {
    UP("up"),
    DOWN("down"),
    LEFT("left"),
    RIGHT("right");

    public final String text;

    public static Direction fromString(String directionString) {
        for (Direction d : Direction.values()) {
            if (d.text.equalsIgnoreCase(directionString)) {
                return d;
            }
        }
        return null;
    }
}
