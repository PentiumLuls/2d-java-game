package com.pentiumluls.common.data;

import lombok.AllArgsConstructor;

/** map of ids for conversion from item to object and vice versa */
@AllArgsConstructor
public enum ObjectToItemMap {
    KEY(0, 0),
    BOOTS(3, 1),
    SEEDS_PARSNIP(4, 2),
    ;

    public int objectId;
    public int itemId;
}
