package com.pentiumluls.common.data;

import com.pentiumluls.client.AppConstants;
import com.pentiumluls.common.data.item.InventorySlot;
import com.pentiumluls.common.data.item.Item;
import com.pentiumluls.common.exception.CanNotRemoveInventoryItem;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Getter
@EqualsAndHashCode
public class PlayerInventory implements Serializable {
    // Slots will eventually become HashMap of: position in inventory and slot (Map<Pair<Integer,Integer>InventorySlot>)
    // so players can sort their inventory
    private List<InventorySlot> slots;
    public int selectorTab; // index of current items page in hotbar(selector)
    public int selectedTileIndex; // index of selected item in hotbar

    public PlayerInventory() {
        slots = new ArrayList<>();
        selectorTab = 0;
        selectedTileIndex = 0;
    }

    public void addItem(Item item, int amount) {
        InventorySlot slot = slots.stream()
                .filter(s -> s.item.equals(item))
                .findFirst()
                .orElse(null);

        if (slot == null) // check if player does not yet have this item
            slots.add(new InventorySlot(item, amount));
        else
            slot.amount += amount;
    }

    public int size() {
        return slots.size();
    }

    public void removeItem(Item item, int amount) throws CanNotRemoveInventoryItem {
        for (int i = 0; i < slots.size(); i++) {
            var slot = slots.get(i);

            if (slot.item.equals(item)) {
                if (slot.amount - amount < 0) {
                    throw new CanNotRemoveInventoryItem("insufficient amount of item to be removed from player's inventory");
                }

                slot.amount -= amount;
                if (slot.amount <= 0) {
                    slots.remove(i);
                }
                return;
            }
        }
        throw new CanNotRemoveInventoryItem("there is no such item in player's inventory");
    }

    public InventorySlot get(int index) {
        return slots.get(index);
    }

    /** return true if currently selected item can be placed on map
     * If there is no item in selected slot, return false */
    public boolean isPlaceAble() {
        return getCurrentSlot()
                .map(inventorySlot -> inventorySlot.item.type.isPlaceAble())
                .orElse(false);
    }

    /** Return null or currently selected item */
    public Optional<Item> getCurrentItem() {
        return getCurrentSlot()
                .map(slot -> slot.item);
    }

    /** Return null or currently selected item slot */
    public Optional<InventorySlot> getCurrentSlot() {
        int index = selectorTab * AppConstants.TILES_IN_SELECTOR + selectedTileIndex;
        if (index >= slots.size())
            return Optional.empty();

        return Optional.of(slots.get(index));
    }
}
