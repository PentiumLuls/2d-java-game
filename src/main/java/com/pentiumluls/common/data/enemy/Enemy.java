package com.pentiumluls.common.data.enemy;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.io.Serial;
import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@EqualsAndHashCode
public class Enemy implements Serializable {
    @Serial private static final long serialVersionUID = 1L;

    public int id;
    public String name;
    public int worldX, worldY; // in pixels, if you want coordinate of tile on map grid, then multiply by TILE_SIZE
    public int health;
    public EnemyType type;

    // TODO: remove it!!! Temp solution
    public static int spriteScale = 2; // Change collisionBox if changing scale :)
}
