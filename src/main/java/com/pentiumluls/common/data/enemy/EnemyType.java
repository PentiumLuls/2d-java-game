package com.pentiumluls.common.data.enemy;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum EnemyType {
    DUMMY("dummy.png"),
    ;

    private final String imageName; // name of file with sprite
}
