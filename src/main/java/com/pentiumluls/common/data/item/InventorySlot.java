package com.pentiumluls.common.data.item;

import lombok.AllArgsConstructor;

import java.io.Serializable;

/** wrapper around Item, so it can be stored in inventory
 * Same multiple items can be stored in one slot*/
@AllArgsConstructor
public class InventorySlot implements Serializable {
    public Item item;
    public int amount;
}
