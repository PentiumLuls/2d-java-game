package com.pentiumluls.common.data.item;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Objects;

/** Can be stored in player's inventory.
 * Can't be put as object/tile directly, but can be converted to object/tile.
 * Also, object can be converted to item when picked up
 * Generic class for all items */
@AllArgsConstructor
@NoArgsConstructor
@Getter
public class Item implements Serializable {
    public int id;
    public int stateId; // for different variations of item. Ex: sword can be new, broken or half-broken
    public String name;
    public String description;
    public ItemType type;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Item item)) return false;
        return id == item.id && stateId == item.stateId && Objects.equals(name, item.name) && Objects.equals(description, item.description) && type == item.type;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, stateId, name, description, type);
    }
}
