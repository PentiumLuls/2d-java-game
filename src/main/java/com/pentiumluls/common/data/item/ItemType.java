package com.pentiumluls.common.data.item;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ItemType {
    KEY(0, "key.png", false),
    BOOTS(1, "boots.png", false),
    SEED_PARSNIP(2, "seed_parsnip.png", true),
    SWORD(3, "weapon/melee/sword.png", false),
    ;

    private final int id;
    private final String imageName; // name of file with sprite
    private final boolean isPlaceAble; // true if item can be placed as object/tile on map

    public static ItemType getById(int id) {
        return ItemType.values()[id];
    }
}
