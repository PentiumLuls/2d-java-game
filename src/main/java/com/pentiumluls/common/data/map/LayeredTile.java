package com.pentiumluls.common.data.map;

/** another layer of tiles, displayed above "Tile" instances.
 * See /doc/Tile.md */
public class LayeredTile extends Tile {
    public int x; // coordinates on tile grid, not in pixels
    public int y;

    public LayeredTile(TileType type) {
        super(type);
    }

    public LayeredTile(TileType type, int stateId) {
        super(type, stateId);
    }

    public LayeredTile(TileType type, int x, int y) {
        super(type);
        this.x = x;
        this.y = y;
    }

    public LayeredTile(TileType type, int stateId, int x, int y) {
        super(type, stateId);
        this.x = x;
        this.y = y;
    }

    public LayeredTile(Tile tile, int x, int y) {
        super(tile.type, tile.stateId);
        this.x = x;
        this.y = y;
    }
}
