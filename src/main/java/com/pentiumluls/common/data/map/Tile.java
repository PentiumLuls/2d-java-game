package com.pentiumluls.common.data.map;

import lombok.NoArgsConstructor;

import java.io.Serializable;

/** Data representation of Tile */
@NoArgsConstructor
public class Tile implements Serializable {
    public int id;
    public TileType type;
    public int stateId = 0; // for different variations of tile. Ex: grass and cornered grass

    public Tile(TileType type) {
        this.id = type.getId();
        this.type = type;
    }

    public Tile(TileType type, int stateId) {
        this.id = type.getId();
        this.type = type;
        this.stateId = stateId;
    }
}

