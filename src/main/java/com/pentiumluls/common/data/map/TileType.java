package com.pentiumluls.common.data.map;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum TileType {
    GRASS(0, "grass.png", false),
    GRASS_PATH(1, "grass_path.png", false),
    WALL(2, "walls.png", true),
    ;

    private final int id;
    private final String imageName; // name of file with sprite
    private final boolean collision;

    public static TileType getById(int id) {
        return TileType.values()[id];
    }
}
