package com.pentiumluls.common.data.map;

import com.pentiumluls.common.data.Pair;
import com.pentiumluls.common.data.object.Object;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@NoArgsConstructor
public class WorldMap implements Serializable {
    public Tile[][] tiles; // 0, 0 tile is up-left corner of screen
    public Map<Pair<Integer, Integer>, LayeredTile> layeredTiles; // key is x,y coords for faster lookup
    public Map<Pair<Integer, Integer>, Object> objects; // key is x,y coords for faster lookup

    public WorldMap(Tile[][] tiles) {
        this.tiles = tiles;
        this.layeredTiles = new HashMap<>();
        this.objects = new HashMap<>();
    }
}
