package com.pentiumluls.common.data.map.subtype;

public enum WallsVariations {
    BIG_SIDE_TOP(0),
    BIG_SIDE_MIDDLE(6),
    BIG_SIDE_BOTTOM(12),

    BIG_SIDE_FRONTSIDE_TOP(18),
    BIG_SIDE_FRONTSIDE_MIDDLE(24),
    BIG_SIDE_FRONTSIDE_BOTTOM(30),

    TOP_LEFT(1),
    TOP_MIDDLE(2),
    TOP_RIGHT(3),

    SIDE_LEFT(7),
    SIDE_RIGHT(9),

    BOTTOM_LEFT(13),
    BOTTOM_MIDDLE(14),
    BOTTOM_RIGHT(15),

    BIG_SIDE_TOP_LEFT_CORNER(4),
    BIG_SIDE_TOP_RIGHT_CORNER(5),
    BIG_SIDE_TOPDOWN_LEFT(10),
    BIG_SIDE_TOPDOWN_RIGHT(11),
    BIG_SIDE_MIDDLE_LEFT(28),
    BIG_SIDE_MIDDLE_RIGHT(29),
    BIG_SIDE_BOTTOM_LEFT_CORNER(34),
    BIG_SIDE_BOTTOM_RIGHT_CORNER(35),

    BIG_TOP_LEFT(19),
    BIG_TOP_MIDDLE(20),
    BIG_TOP_RIGHT(21),
    BIG_MIDDLE_LEFT(25),
    BIG_MIDDLE_MIDDLE(26),
    BIG_MIDDLE_RIGHT(27),
    BIG_BOTTOM_LEFT(31),
    BIG_BOTTOM_MIDDLE(32),
    BIG_BOTTOM_RIGHT(33),
    ;

    public final int stateId; // index to sprite in tile spritesheet

    WallsVariations(int tileId) {
        this.stateId = tileId;
    }
}
