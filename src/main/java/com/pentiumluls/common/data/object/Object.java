package com.pentiumluls.common.data.object;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;

@AllArgsConstructor
@EqualsAndHashCode
public class Object implements Serializable {
    @Serial
    private static final long serialVersionUID = 1L;

    public String name;
    public ObjectType type;
    public int stateId = 0; // for different variations of tile. Ex: grass and cornered grass

    public Object(String name, ObjectType type) {
        this.name = name;
        this.type = type;
    }
}
