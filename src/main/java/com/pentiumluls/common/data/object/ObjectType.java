package com.pentiumluls.common.data.object;

import com.pentiumluls.client.AppConstants;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.awt.*;

@Getter
@AllArgsConstructor
public enum ObjectType {
    KEY(0, "key.png", false),
    TENT(1, "tent.png", true),
    DOOR(2, "door.png", true),
    BOOTS(3, "boots.png", false),
    SEED_PARSNIP(4, "seed_parsnip.png", false),
    CROP_PARSNIP(5, "crops_parsnip.png", false),
    ;

    private final int id;
    private final String imageName; // name of file with sprite
    private final boolean collision; // true if object prevents from moving
    private final Rectangle collisionBox; // relative to original object sprite rectangle. For interaction with player

    ObjectType(int id, String imageName, boolean collision) {
        this.id = id;
        this.imageName = imageName;
        this.collision = collision;
        // set default collision to whole tile size
        this.collisionBox = new Rectangle(0, 0, AppConstants.TILE_SIZE, AppConstants.TILE_SIZE);
    }

    public static ObjectType getById(int id) {
        return ObjectType.values()[id];
    }
}
