package com.pentiumluls.common.data.object.subtype;

import com.pentiumluls.common.data.object.Object;
import com.pentiumluls.common.data.object.ObjectType;
import com.pentiumluls.common.data.object.PickableObject;

public class BootsObject extends Object implements PickableObject {
    public int speedBonus = 2;

    public BootsObject(String name, ObjectType type) {
        super(name, type);
    }

    public BootsObject(String name, int speedBoost, ObjectType type) {
        super(name, type);
        this.speedBonus = speedBoost;
    }
}
