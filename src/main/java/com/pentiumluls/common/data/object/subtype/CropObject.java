package com.pentiumluls.common.data.object.subtype;

import com.pentiumluls.common.data.object.Object;
import com.pentiumluls.common.data.object.ObjectType;

public class CropObject extends Object {
    private int growStage; // different stages of crop growth. Need to be in sync with stateId field of Object
    private final int FIRST_GROW_STAGE = 1;
    private final int LAST_GROW_STAGE = 5;

    public CropObject(String name, ObjectType type) {
        super(name, type);
        setGrowStage(FIRST_GROW_STAGE);
    }

    public int getGrowStage() {
        return growStage;
    }

    public void setGrowStage(int growStage) {
        this.growStage = growStage;
        this.stateId = growStage;
    }

    public void incrementGrowthStage() {
        if (growStage != LAST_GROW_STAGE) {
            setGrowStage(growStage + 1);
        }
    }
}
