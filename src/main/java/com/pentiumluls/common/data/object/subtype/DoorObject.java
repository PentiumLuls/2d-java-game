package com.pentiumluls.common.data.object.subtype;

import com.pentiumluls.common.data.object.Object;
import com.pentiumluls.common.data.object.ObjectType;

public class DoorObject extends Object {
    public final int doorId; // for unlocking
    public boolean isOpen = false;

    public DoorObject(String name, int doorId, ObjectType type) {
        super(name, type);
        this.doorId = doorId;
    }
    public DoorObject(String name, int doorId, boolean isOpen, ObjectType type) {
        super(name, type);
        this.doorId = doorId;
        this.isOpen = isOpen;
    }
}
