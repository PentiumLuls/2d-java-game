package com.pentiumluls.common.data.object.subtype;

import com.pentiumluls.common.data.object.Object;
import com.pentiumluls.common.data.object.ObjectType;
import com.pentiumluls.common.data.object.PickableObject;

public class KeyObject extends Object implements PickableObject {
    public final int doorId; // for unlocking

    public KeyObject(String name, int doorId, ObjectType type) {
        super(name, type);
        this.doorId = doorId;
    }
}
