package com.pentiumluls.common.data.object.subtype;

import com.pentiumluls.common.data.object.Object;
import com.pentiumluls.common.data.object.ObjectType;
import com.pentiumluls.common.data.object.PickableObject;

public class SeedsObject extends Object implements PickableObject {

    public SeedsObject(String name, ObjectType type) {
        super(name, type);
    }
}
