package com.pentiumluls.common.exception;

public class CanNotPlaceItemException extends Exception {
    public CanNotPlaceItemException(String message) {
        super(message);
    }
}
