package com.pentiumluls.common.exception;

public class CanNotRemoveInventoryItem extends Exception {
    public CanNotRemoveInventoryItem(String message) {
        super(message);
    }
}
