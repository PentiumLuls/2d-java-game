package com.pentiumluls.common.io;

import com.pentiumluls.client.AppConstants;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

public class ImageUtils {
    /** returns new scaled images.
     * Use it for optimization (scale image once at init) */
    public static BufferedImage scaleImage(BufferedImage image, int width, int height) {
        BufferedImage scaled = new BufferedImage(width, height, image.getType());
        Graphics2D g2 = scaled.createGraphics();
        g2.drawImage(image, 0, 0, width, height, null);
        g2.dispose();
        return scaled;
    }

    /** split 'sheet' into images(size determined by "AppConstants.ORIGINAL_TILE_SIZE"
     * return list of new images. Sheet is not mutated */
    public static List<BufferedImage> loadSpritesFromImage(BufferedImage sheet) {
        int columns = sheet.getWidth() / AppConstants.ORIGINAL_TILE_SIZE;
        int rows = sheet.getHeight() / AppConstants.ORIGINAL_TILE_SIZE;
        List<BufferedImage> sprites = new ArrayList<>();
        for (int row = 0; row < rows; row++) {
            for (int col = 0; col < columns; col++) {
                //create sub image
                BufferedImage image = sheet.getSubimage(col * AppConstants.ORIGINAL_TILE_SIZE, row * AppConstants.ORIGINAL_TILE_SIZE,
                        AppConstants.ORIGINAL_TILE_SIZE, AppConstants.ORIGINAL_TILE_SIZE);

                sprites.add(image);
            }
        }
        return sprites;
    }

}
