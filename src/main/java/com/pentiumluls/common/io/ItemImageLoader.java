package com.pentiumluls.common.io;

import com.pentiumluls.client.AppConstants;
import com.pentiumluls.common.data.item.Item;
import com.pentiumluls.common.data.item.ItemType;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Singleton class.
 * Primary purpose is optimisation - so we do not store image in each object.
 * Instead, we load one image once at constructor
 */
public class ItemImageLoader {
    private static ItemImageLoader instance;

    // Lists of each sprite variation for each tile sprite sheet
    protected List<List<BufferedImage>> images; // not scaled
    private List<List<BufferedImage>> inventoryImages; // scaled to tile size
    private List<List<BufferedImage>> heldItemImages; // scaled to tile size

    public static ItemImageLoader instance() {
        if (instance == null)
            instance = new ItemImageLoader();
        return instance;
    }

    private ItemImageLoader() {
        preloadAllImages();
        scaleAllImages();
    }

    /** return item image scaled to same size as player/tile sprites
     * Used for displaying in inventory */
    public BufferedImage getImageForInventory(Item item) {
        return inventoryImages.get(item.getId())
                .get(item.stateId);
    }

    /** return item image scaled to "AppConstants.HELD_ITEM_SIZE"
     * Used for displaying when held by player */
    public BufferedImage getImageForHolding(Item item) {
        return heldItemImages.get(item.getId())
                .get(item.stateId);
    }

    /** return original item image to same size as player/tile sprites
     * Used for displaying item, held by player */
    public BufferedImage getImage(Item item) {
        return images.get(item.getId())
                .get(item.stateId);
    }

    private void preloadAllImages() {
        images = new ArrayList<>();
        try {
            for (int i = 0; i < ItemType.values().length; i++) {
                ItemType type = ItemType.values()[i];
                BufferedImage sheet = ImageIO.read(getClass().getResourceAsStream("/items/" + type.getImageName()));
                images.add(ImageUtils.loadSpritesFromImage(sheet));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /** copy images list into new, containing scaled images */
    private void scaleAllImages() {
        inventoryImages = new ArrayList<>();
        heldItemImages = new ArrayList<>();
        for (int i = 0; i < images.size(); i++) {
            List<BufferedImage> tileVariations = images.get(i);
            inventoryImages.add(new ArrayList<>());
            heldItemImages.add(new ArrayList<>());
            for (int j = 0; j < tileVariations.size(); j++) {
                inventoryImages.get(i).add(
                        ImageUtils.scaleImage(tileVariations.get(j), AppConstants.TILE_SIZE, AppConstants.TILE_SIZE));

                heldItemImages.get(i).add(
                        ImageUtils.scaleImage(tileVariations.get(j), AppConstants.HELD_ITEM_SIZE, AppConstants.HELD_ITEM_SIZE));
            }
        }
    }
}
