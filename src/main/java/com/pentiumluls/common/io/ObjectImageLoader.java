package com.pentiumluls.common.io;

import com.pentiumluls.client.AppConstants;
import com.pentiumluls.common.data.object.Object;
import com.pentiumluls.common.data.object.ObjectType;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Primary purpose is optimisation - so we do not store image in each object.
 * Instead, we load one image once at constructor
 */
public class ObjectImageLoader {
    protected List<List<BufferedImage>> images; // List of each sprite variation for each tile sprite sheet

    public ObjectImageLoader() {
        preloadAllImages();
        scaleAllImages();
    }

    public BufferedImage getImage(Object object) {
        return images.get(object.type.getId())
                .get(object.stateId);
    }

    private void preloadAllImages() {
        images = new ArrayList<>();
        try {
            for (int i = 0; i < ObjectType.values().length; i++) {
                ObjectType objectType = ObjectType.values()[i];
                BufferedImage sheet = ImageIO.read(getClass().getResourceAsStream("/objects/" + objectType.getImageName()));
                images.add(ImageUtils.loadSpritesFromImage(sheet));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /** mutate original images list with new, scaled images */
    private void scaleAllImages() {
        List<List<BufferedImage>> scaledImages = new ArrayList<>();
        for (List<BufferedImage> tileVariations : images) {
            ArrayList<BufferedImage> types = new ArrayList<>();
            for (int i = 0; i < tileVariations.size(); i++) {
                BufferedImage scaledImage = ImageUtils.scaleImage(tileVariations.get(i), AppConstants.TILE_SIZE, AppConstants.TILE_SIZE);
                types.add(scaledImage);
            }
            scaledImages.add(types);
        }
        images = scaledImages;
    }
}
