package com.pentiumluls.common.logic;

/** Implements Command design pattern */
public interface Command {
    public void execute();
}
