package com.pentiumluls.common.net.packets;

import lombok.AllArgsConstructor;

import java.io.Serializable;

@AllArgsConstructor
public class ChatMessagePacket extends Packet implements Serializable {
    public String message;
    public String userName;
}
