package com.pentiumluls.common.net.packets;

import com.pentiumluls.common.data.Pair;
import com.pentiumluls.common.data.object.Object;
import lombok.AllArgsConstructor;

import java.io.Serializable;
import java.util.HashMap;

@AllArgsConstructor
public class NewDayPacket extends Packet implements Serializable {
    public HashMap<Pair<Integer, Integer>, Object> changedObjects;
}
