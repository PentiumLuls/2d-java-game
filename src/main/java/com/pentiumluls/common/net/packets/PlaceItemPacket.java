package com.pentiumluls.common.net.packets;

import com.pentiumluls.common.data.item.Item;
import lombok.AllArgsConstructor;

import java.io.Serializable;

@AllArgsConstructor
public class PlaceItemPacket extends Packet implements Serializable {
    public Item placedItem;
    public int worldX; // tile indexes, not in pixels
    public int worldY;
}
