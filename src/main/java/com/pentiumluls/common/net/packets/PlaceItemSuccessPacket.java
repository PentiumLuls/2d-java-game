package com.pentiumluls.common.net.packets;

import com.pentiumluls.common.data.item.Item;
import com.pentiumluls.common.data.object.Object;
import lombok.AllArgsConstructor;

import java.io.Serializable;

@AllArgsConstructor
public class PlaceItemSuccessPacket extends Packet implements Serializable {
    public int playerId;
    public Item item;
    public Object object;
    public int worldX; // tile indexes, not in pixels
    public int worldY;
}
