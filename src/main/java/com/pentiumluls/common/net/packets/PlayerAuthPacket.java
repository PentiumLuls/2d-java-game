package com.pentiumluls.common.net.packets;

import lombok.AllArgsConstructor;

import java.io.Serializable;

@AllArgsConstructor
public class PlayerAuthPacket extends Packet implements Serializable {
    public String username;
    public int id; // id = -1 if player not registered yet
}
