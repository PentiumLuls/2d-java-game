package com.pentiumluls.common.net.packets;

import lombok.AllArgsConstructor;

import java.io.Serializable;

@AllArgsConstructor
public class PlayerDisconnectPacket extends Packet implements Serializable {
    public int playerId;
}
