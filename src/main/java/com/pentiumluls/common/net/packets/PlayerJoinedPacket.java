package com.pentiumluls.common.net.packets;

import com.pentiumluls.common.net.transferdata.PlayerTransferData;
import lombok.AllArgsConstructor;

import java.io.Serializable;

@AllArgsConstructor
public class PlayerJoinedPacket extends Packet implements Serializable {
    public PlayerTransferData player;
}
