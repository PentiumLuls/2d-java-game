package com.pentiumluls.common.net.packets;

import com.pentiumluls.common.data.Direction;
import lombok.AllArgsConstructor;

import java.io.Serializable;

/** Tell server that player with playerId is moving in specified direction
 * (not exactly moved yet. See flow diagrams in /doc) */
@AllArgsConstructor
public class PlayerMovePacket extends Packet implements Serializable {
    public int playerId;
    public Direction direction;
}
