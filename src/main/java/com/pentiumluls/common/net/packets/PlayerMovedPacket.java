package com.pentiumluls.common.net.packets;

import com.pentiumluls.common.data.Direction;
import lombok.AllArgsConstructor;

import java.io.Serializable;

@AllArgsConstructor
public class PlayerMovedPacket extends Packet implements Serializable {
    public int playerId;
    public Direction direction;
    public int worldX;
    public int worldY;
}
