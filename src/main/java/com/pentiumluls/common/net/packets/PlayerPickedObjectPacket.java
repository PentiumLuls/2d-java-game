package com.pentiumluls.common.net.packets;

import com.pentiumluls.common.data.item.Item;
import lombok.AllArgsConstructor;

import java.io.Serializable;

@AllArgsConstructor
public class PlayerPickedObjectPacket extends Packet implements Serializable {
    public int playerId;
    public Item item;
    public int amount;
    public int x;
    public int y;
}
