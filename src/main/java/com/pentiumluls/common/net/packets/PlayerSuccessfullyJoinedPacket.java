package com.pentiumluls.common.net.packets;

import com.pentiumluls.common.data.PlayerInventory;
import com.pentiumluls.common.data.enemy.Enemy;
import com.pentiumluls.common.data.map.WorldMap;
import com.pentiumluls.common.net.transferdata.PlayerTransferData;
import lombok.AllArgsConstructor;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

@AllArgsConstructor
public class PlayerSuccessfullyJoinedPacket extends Packet implements Serializable {
    public PlayerTransferData player;
    public List<PlayerTransferData> onlinePlayers;
    public WorldMap map;
    public PlayerInventory inventory;
    public Map<Integer, Enemy> enemies;
}
