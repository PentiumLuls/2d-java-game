package com.pentiumluls.common.net.transferdata;

import com.pentiumluls.common.data.Direction;

import java.io.Serializable;

public class PlayerTransferData implements Serializable {
    public int id;
    public String name;
    public int worldX, worldY;
    public Direction direction;

    /** converts from server player model to transfer object */
    public PlayerTransferData(com.pentiumluls.server.data.Player player) {
        id = player.getId();
        name = player.getName();
        worldX = player.getWorldX();
        worldY = player.getWorldY();
        direction = player.getDirection();
    }

    /** converts from client player model to transfer object */
    public PlayerTransferData(com.pentiumluls.client.data.entity.Player player) {
        id = player.id;
        name = player.username;
        worldX = player.worldX;
        worldY = player.worldY;
        direction = player.direction;
    }
}
