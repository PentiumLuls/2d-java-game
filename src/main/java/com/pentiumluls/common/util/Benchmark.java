package com.pentiumluls.common.util;

import com.pentiumluls.common.util.logger.Logger;

public class Benchmark {
    private long startTime;
    private long endTime;
    private String operationName;

    /** start measuring
     * @param operationName - for logging the name of measurement
     * Should not be called twice before stopping - instead create another instance of Benchmark
     * */
    public void start(String operationName) {
        this.operationName = operationName;
        startTime = System.nanoTime();
    }

    public void stop() {
        endTime = System.nanoTime();
        long ellapsedTime = (endTime - startTime);

        Logger.log(this.getClass().getSimpleName(), (operationName + ": " + ellapsedTime));
    }
}
