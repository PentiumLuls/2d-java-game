package com.pentiumluls.common.util;

import com.pentiumluls.common.data.item.Item;
import com.pentiumluls.common.data.item.ItemType;

public class ItemBuilder {
    private Item item = new Item();

    public ItemBuilder of(ItemType type) {
        item.type = type;
        item.id = type.getId();
        setDefaultValues();
        return this;
    }

    private void setDefaultValues() {
        item.stateId = 0;
        item.name = "";
        item.description = "";
    }

    public Item build() {
        return item;
    }
}
