package com.pentiumluls.common.util.logger;

public class Logger {

    /** @param className - name of class that called */
    public static void log(String className, String stringToLog) {
        System.out.println("[ " + className + " ]   " + stringToLog );
    }

    /** @param layer - application layer which is logged */
    public static void log(LoggerLayer layer, String stringToLog) {
        System.out.println("[ " + layer.name + " ]   " + stringToLog );
    }
}
