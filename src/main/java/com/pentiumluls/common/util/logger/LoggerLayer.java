package com.pentiumluls.common.util.logger;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public enum LoggerLayer {
    LOGIC("LOCIC"),
    NETWORK("NETWORK"),
    DATA("DATA"),
    IO("IO"),
    EVENT_POOL("EVENT_POOL"),
    MAP_EDITOR("MAP_EDITOR");

    public String name;
}
