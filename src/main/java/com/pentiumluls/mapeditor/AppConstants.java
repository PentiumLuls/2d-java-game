package com.pentiumluls.mapeditor;

public class AppConstants {
    public static final int FPS = 60; // Ticks Per Second
    public static final int TILE_SIZE = 48; // 48x48 pixels each tile
    public static final int MAX_SCREEN_COL = 20; // how many tiles are fit to screen
    public static final int MAX_SCREEN_ROW = 14; // how many tiles are fit to screen
    public static final int SCREEN_WIDTH = TILE_SIZE * MAX_SCREEN_COL; // 960 pixels
    public static final int SCREEN_HEIGHT = TILE_SIZE * MAX_SCREEN_ROW; // 672 pixels Height of map screen, without selector

    public static final int SELECTOR_HEIGHT = TILE_SIZE + 5; // with border
    public static final int TILES_IN_SELECTOR = 18; // visible tile types in selector
    public static final int SELECTOR_BORDER = 3;

    public static String SAVE_DIR = System.getProperty("user.home") + "/.pentiumluls-server/";
    public static String MAPS_DIR = SAVE_DIR + "maps/";

    // specify how many game loops to scip before next interaction with mouse can be done.
    // Ex: if user pressed right mouse, only one top tile will be removed
    // instead of removing it 7 times. (Because, even a short click produces many events).
    // This way we can draw without releasing mouse
    public static int MOUSE_INTERACT_DELAY = 3;
}
