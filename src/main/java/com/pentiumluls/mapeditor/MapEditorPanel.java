package com.pentiumluls.mapeditor;

import com.pentiumluls.common.data.Pair;
import com.pentiumluls.common.data.map.LayeredTile;
import com.pentiumluls.common.data.map.Tile;
import com.pentiumluls.common.data.map.TileType;
import com.pentiumluls.common.data.map.WorldMap;
import com.pentiumluls.common.data.map.subtype.GrassVariations;
import com.pentiumluls.common.data.map.subtype.WallsVariations;
import com.pentiumluls.common.data.object.Object;
import com.pentiumluls.common.data.object.ObjectType;
import com.pentiumluls.common.data.object.subtype.*;
import com.pentiumluls.common.io.ObjectImageLoader;
import com.pentiumluls.common.io.TileImageLoader;
import com.pentiumluls.common.util.logger.Logger;
import com.pentiumluls.common.util.logger.LoggerLayer;
import com.pentiumluls.mapeditor.io.KeyHandler;
import com.pentiumluls.mapeditor.io.MouseHandler;
import com.pentiumluls.mapeditor.io.MouseWheelHandler;
import com.pentiumluls.mapeditor.smart_tiling.SmartTilingGrassPathMapping;
import com.pentiumluls.server.io.map.MapFileLoader;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class MapEditorPanel extends JPanel implements Runnable {
    Thread gameThread;
    private TileImageLoader imageLoader = new TileImageLoader();
    private final ObjectImageLoader objectImageLoader = new ObjectImageLoader();

    MapFileLoader mapLoader = new MapFileLoader();
    KeyHandler keyHandler = new KeyHandler(this);
    MouseHandler mouseHandler = new MouseHandler();
    MouseWheelHandler mouseWheelHandler = new MouseWheelHandler(this);

    int worldX = 10 * AppConstants.TILE_SIZE;
    int worldY = 10 * AppConstants.TILE_SIZE;
    int screenX = (AppConstants.SCREEN_WIDTH / 2) - (AppConstants.TILE_SIZE / 2);
    int screenY = (AppConstants.SCREEN_HEIGHT / 2) - (AppConstants.TILE_SIZE / 2);
    int speed = 10;

    int selectorTab = 0;
    int selectorTabMax;
    int selectedTileIndex = 0;
    List<Tile> allTiles = new ArrayList<>();
    List<Object> allObjects = new ArrayList<>();

    WorldMap map;
    int mapWidth, mapHeight;

    int leftMouseDelayCounter = AppConstants.MOUSE_INTERACT_DELAY;
    int rightMouseDelayCounter = AppConstants.MOUSE_INTERACT_DELAY;

    public MapEditorPanel() {
        JFrame window = new JFrame();
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        window.setResizable(false);
        window.setTitle("MAP EDITOR");

        window.add(this);
        window.pack(); // set main window size to fit subcomponents
        window.setLocationRelativeTo(null);
        window.setVisible(true);
        window.setSize(new Dimension(AppConstants.SCREEN_WIDTH,
                AppConstants.SCREEN_HEIGHT + AppConstants.SELECTOR_HEIGHT));
        setBackground(Color.BLACK);
        setDoubleBuffered(true); // increase rendering performance
        window.setFocusable(true);

        window.addKeyListener(keyHandler);
        window.addMouseListener(mouseHandler);
        window.addMouseWheelListener(mouseWheelHandler);

        // Init all tiles for selector
        for (TileType type : TileType.values()) {
            switch (type) {
                case GRASS_PATH -> {
                    allTiles.add(new Tile(type, GrassVariations.PATH_MIDDLE.stateId));
                }
                case WALL -> {
                    for (WallsVariations variation : WallsVariations.values()) {
                        allTiles.add(new LayeredTile(TileType.WALL, variation.stateId));
                    }
                }
                default -> allTiles.add(new Tile(type));
            }
        }
        for (ObjectType type : ObjectType.values()) {
            switch (type) {
                case KEY -> allObjects.add(new KeyObject(type.name(), 0, type));
                case DOOR -> allObjects.add(new DoorObject(type.name(), 0, type));
                case BOOTS -> allObjects.add(new BootsObject(type.name(), 2, type));
                case SEED_PARSNIP -> allObjects.add(new SeedsObject(type.name(), type));
                case CROP_PARSNIP -> allObjects.add(new CropObject(type.name(), type));
                default -> allObjects.add(new Object(type.name(), type));
            }
        }

        // selector contains both tiles and objects
        selectorTabMax = (allTiles.size() + allObjects.size()) / AppConstants.TILES_IN_SELECTOR;

        startGameThread();
    }

    public void startGameThread() {
        gameThread = new Thread(this);
        gameThread.start();
    }

    @Override
    public void run() {
        loadMap();

        double drawInterval = 1000000000 / AppConstants.FPS;
        double delta = 0;
        long lastTime = System.nanoTime();
        long currentTime;
        long timer = 0;
        int drawCount = 0;

        while (gameThread != null) {
            currentTime = System.nanoTime();
            delta += (currentTime - lastTime) / drawInterval;
            timer += (currentTime - lastTime);
            lastTime = currentTime;

            if (delta >= 1) {
                // ACTUAL GAME LOOP, FRAMED IN PREDEFINED FPS
                update();
                repaint();

                delta--;
                drawCount++;
            }
            if (timer >= 1000000000) {
                System.out.println("FPS: " + drawCount);
                drawCount = 0;
                timer = 0;
            }
        }
    }

    public void update() {
        // MOVEMENT
        if (keyHandler.upPressed) {
            worldY -= speed;
        }
        if (keyHandler.downPressed) {
            worldY += speed;
        }
        if (keyHandler.leftPressed) {
            worldX -= speed;
        }
        if (keyHandler.rightPressed) {
            worldX += speed;
        }

        // TILE EDITING
        leftMouseDelayCounter--;
        rightMouseDelayCounter--;
        if (leftMouseDelayCounter == 0)
            leftMouseDelayCounter = AppConstants.MOUSE_INTERACT_DELAY;
        if (rightMouseDelayCounter == 0)
            rightMouseDelayCounter = AppConstants.MOUSE_INTERACT_DELAY;

        if (!mouseHandler.isLeftClicked && !mouseHandler.isRightClicked) {
            leftMouseDelayCounter = AppConstants.MOUSE_INTERACT_DELAY;
            rightMouseDelayCounter = AppConstants.MOUSE_INTERACT_DELAY;
            return; // return if no mouse button pressed
        }

        Point mousePosition = getMousePosition();
        if (mousePosition == null)
            return;
        int mouseX = mousePosition.x;
        int mouseY = mousePosition.y;
        if (mouseY > getHeight() - AppConstants.SELECTOR_HEIGHT) {
            // clicked on selector
            selectedTileIndex = getSelectorIndex(mouseX);

        } else { // clicked on map
            // See AppConstants.MOUSE_INTERACT_DELAY for explanation
            if (mouseHandler.isLeftClicked && leftMouseDelayCounter != AppConstants.MOUSE_INTERACT_DELAY) {
                return;
            }
            if (mouseHandler.isRightClicked && rightMouseDelayCounter != AppConstants.MOUSE_INTERACT_DELAY) {
                return;
            }

            var cords = getHoveredTileCoordinates(mouseX, mouseY);
            if (mouseHandler.isLeftClicked && cords != null)
                place(cords.getFirst(), cords.getSecond());

            else if (mouseHandler.isRightClicked && cords != null)
                remove(cords.getFirst(), cords.getSecond());
        }
    }

    /** removes one top object/tile from map at specified coordinates */
    private void remove(int x, int y) {
        Pair<Integer, Integer> key = new Pair<>(x, y);

        // check first for objects
        if (map.objects.containsKey(key)) {
            map.objects.remove(key);
            return;
        }
        // check for layered tiles
        if (map.layeredTiles.containsKey(key)) {
            map.layeredTiles.remove(key);
            return;
        }
        //check for basic tiles
        map.tiles[x][y] = new Tile(TileType.GRASS);
    }

    private void place(int column, int row) {
        try {
            int startIndex = AppConstants.TILES_IN_SELECTOR;
            int index = selectorTab * startIndex + selectedTileIndex;
            if (index < allTiles.size()) {
                placeTile(column, row, index);
            } else {
                placeObject(column, row, index - allTiles.size());
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            log("Can not paint at " + column + ":" + row);
        }
    }

    private void placeTile(int column, int row, int index) {
        Tile selectedTile = allTiles.get(index);

        if (selectedTile instanceof LayeredTile) {
            map.layeredTiles.put(new Pair<>(column, row), new LayeredTile(selectedTile, column, row));
        } else {
            switch (selectedTile.type) {
                case GRASS_PATH -> placeGrassPath(column, row, true);
                default -> map.tiles[column][row] = selectedTile;
            }
        }
    }

    /** Place grass path in specified coordinates.
     * Also changes neighbouring tiles if they also grass path
     * @param isRecursive - true if called outside of this same method
     * Implementation of "Tile Bitmasking". See an explanation:
     * https://gamedevelopment.tutsplus.com/tutorials/how-to-use-tile-bitmasking-to-auto-tile-your-level-layouts--cms-25673 */
    private void placeGrassPath(int x, int y, boolean isRecursive) {
        Tile topNeighbour = map.tiles[x][y-1];
        Tile bottomNeighbour = map.tiles[x][y+1];
        Tile leftNeighbour = map.tiles[x-1][y];
        Tile rightNeighbour = map.tiles[x+1][y];
        Tile topLeftNeighbour = map.tiles[x-1][y-1];
        Tile topRightNeighbour = map.tiles[x+1][y-1];
        Tile bottomLeftNeighbour = map.tiles[x-1][y+1];
        Tile bottomRightNeighbour = map.tiles[x+1][y+1];

        int top = (topNeighbour.type == TileType.GRASS_PATH) ? 1 : 0;
        int bottom = (bottomNeighbour.type == TileType.GRASS_PATH) ? 1 : 0;
        int left = (leftNeighbour.type == TileType.GRASS_PATH) ? 1 : 0;
        int right = (rightNeighbour.type == TileType.GRASS_PATH) ? 1 : 0;
        int topLeft = (topLeftNeighbour.type == TileType.GRASS_PATH) ? 1 : 0;
        int topRight = (topRightNeighbour.type == TileType.GRASS_PATH) ? 1 : 0;
        int bottomLeft = (bottomLeftNeighbour.type == TileType.GRASS_PATH) ? 1 : 0;
        int bottomRight = (bottomRightNeighbour.type == TileType.GRASS_PATH) ? 1 : 0;

        int mask = (2*top) + (8*left) + (16 * right) + (64 * bottom);

        if (top != 0 && left != 0)
            mask += topLeft;
        if (top != 0 && right != 0)
            mask += (4*topRight);
        if (bottom != 0 && left != 0)
            mask += (32 * bottomLeft);
        if (bottom != 0 && right != 0)
            mask += (128 * bottomRight);

        if (isRecursive) {
            map.tiles[x][y] = new Tile(TileType.GRASS_PATH, SmartTilingGrassPathMapping.getVariation(mask).stateId);
        } else if ( map.tiles[x][y].type == TileType.GRASS_PATH) {
            // update neighbours in recursive call only if it's grass path
            map.tiles[x][y] = new Tile(TileType.GRASS_PATH, SmartTilingGrassPathMapping.getVariation(mask).stateId);
        }

        // update neighbouring tiles the same way, but pass false to prevent infinite recursion
        if (isRecursive) {
            placeGrassPath(x, y - 1, false);
            placeGrassPath(x, y + 1, false);
            placeGrassPath(x - 1, y, false);
            placeGrassPath(x + 1, y, false);
            placeGrassPath(x - 1, y - 1, false);
            placeGrassPath(x - 1, y + 1, false);
            placeGrassPath(x + 1, y - 1, false);
            placeGrassPath(x + 1, y + 1, false);
        }
    }

    private void placeObject(int column, int row, int index) {
        ObjectType type = allObjects.get(index).type;
        // TODO: refactor object creation in mapEditor
        switch (type) {
            case KEY -> map.objects.put(new Pair<>(column, row), new KeyObject(type.name(), 0, type));
            case DOOR -> map.objects.put(new Pair<>(column, row), new DoorObject(type.name(), 0, type));
            case BOOTS -> map.objects.put(new Pair<>(column, row), new BootsObject(type.name(), 2, type));
            case SEED_PARSNIP -> map.objects.put(new Pair<>(column, row), new SeedsObject(type.name(), type));
            case CROP_PARSNIP -> map.objects.put(new Pair<>(column, row), new CropObject(type.name(), type));
            default -> map.objects.put(new Pair<>(column, row), new Object(type.name(), type));
        }
    }

    // called on repaint()
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;

        drawMap(g2);
        drawHover(g2);
        drawSelector(g2);

        g2.dispose();
    }

    private void drawMap(Graphics2D g2) {
        if (map == null)
            return;

        drawBasicTiles(g2);
        drawLayeredTiles(g2);
        drawObjects(g2);
    }

    private void drawBasicTiles(Graphics2D g2) {
        for (int column = 0; column < mapWidth; column++) {
            for (int row = 0; row < mapHeight; row++) {
                Tile tile = map.tiles[column][row];
                int worldX = column * AppConstants.TILE_SIZE;
                int worldY = row * AppConstants.TILE_SIZE;
                int screenX = worldX - this.worldX + this.screenX;
                int screenY = worldY - this.worldY + this.screenY;

                // draw if only displayed on screen
                if (screenX > (-AppConstants.TILE_SIZE) && screenX < AppConstants.SCREEN_WIDTH
                        && screenY > (-AppConstants.TILE_SIZE) && screenY < AppConstants.SCREEN_HEIGHT) {
                    g2.drawImage(imageLoader.getImage(tile),
                            screenX, screenY, null);

//                    drawTileCoordinate(g2, column, row, screenX, screenY);
//                    drawGrid(g2, screenX, screenY);
                }
            }
        }
    }

    private void drawLayeredTiles(Graphics2D g2) {
        for (LayeredTile tile : map.layeredTiles.values()) {
            int worldX = tile.x * AppConstants.TILE_SIZE;
            int worldY = tile.y * AppConstants.TILE_SIZE;
            int screenX = worldX - this.worldX + this.screenX;
            int screenY = worldY - this.worldY + this.screenY;

            // draw if only displayed on screen
            if (screenX > (-AppConstants.TILE_SIZE) && screenX < AppConstants.SCREEN_WIDTH
                    && screenY > (-AppConstants.TILE_SIZE) && screenY < AppConstants.SCREEN_HEIGHT) {
                g2.drawImage(imageLoader.getImage(tile),
                        screenX, screenY, null);

//                drawTileCoordinate(g2, tile.x, tile.y, screenX, screenY);
//                    drawGrid(g2, screenX, screenY);
            }

        }
    }

    private void drawObjects(Graphics2D g2) {
        for (Map.Entry<Pair<Integer, Integer>, Object> entry : map.objects.entrySet()) {
            int worldX = entry.getKey().getFirst() * AppConstants.TILE_SIZE;
            int worldY = entry.getKey().getSecond() * AppConstants.TILE_SIZE;
            int screenX = worldX - this.worldX + this.screenX;
            int screenY = worldY - this.worldY + this.screenY;

            // draw if only displayed on screen
            if (screenX > (-AppConstants.TILE_SIZE) && screenX < AppConstants.SCREEN_WIDTH
                    && screenY > (-AppConstants.TILE_SIZE) && screenY < AppConstants.SCREEN_HEIGHT) {
                g2.drawImage(objectImageLoader.getImage(entry.getValue()),
                        screenX, screenY, null);
            }

        }
    }

    private void drawHover(Graphics2D g2) {
        Point mousePosition = getMousePosition();
        if (mousePosition == null)
            return;
        int mouseX = mousePosition.x;
        int mouseY = mousePosition.y;

        var cords = getHoveredTileCoordinates(mouseX, mouseY);
        if (cords == null)
            return;
        int column = cords.getFirst();
        int row = cords.getSecond();

        // tile position on screen
        int tileX = column * AppConstants.TILE_SIZE - this.worldX + this.screenX;
        int tileY = row * AppConstants.TILE_SIZE - this.worldY + this.screenY;

        Rectangle rect = new Rectangle(tileX, tileY, AppConstants.TILE_SIZE, AppConstants.TILE_SIZE);
        Composite composite = g2.getComposite();
        g2.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.5f));
        g2.fill(rect);
        g2.setComposite(composite);
    }

    private void drawSelector(Graphics2D g2) {
        int y = getHeight() - AppConstants.SELECTOR_HEIGHT;
        // background
        g2.setColor(Color.GRAY);
        g2.fillRect(0, y,
                AppConstants.SCREEN_WIDTH, AppConstants.SELECTOR_HEIGHT);

        // tiles and objects
        int startIndex = selectorTab * AppConstants.TILES_IN_SELECTOR;
        g2.setColor(Color.BLACK);
        g2.setStroke(new BasicStroke(3.0f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER, 10.0f));
        int maxIndex = allTiles.size() + allObjects.size() - 1;

        for (int row = 0; row < AppConstants.TILES_IN_SELECTOR; row++) {
            int index = startIndex + row;
            if (index > maxIndex)
                break;

            int screenX = row * (AppConstants.TILE_SIZE + AppConstants.SELECTOR_BORDER);
            g2.drawRect(screenX, y,
                    AppConstants.TILE_SIZE + AppConstants.SELECTOR_BORDER, AppConstants.TILE_SIZE + AppConstants.SELECTOR_BORDER);

            BufferedImage image;
            if (index < allTiles.size()) {
                // it's tile
                image = imageLoader.getImage(allTiles.get(index));
            } else {
                // object
                image = objectImageLoader.getImage(allObjects.get(index - allTiles.size()));
            }

            g2.drawImage(image,
                    screenX + AppConstants.SELECTOR_BORDER, y + AppConstants.SELECTOR_BORDER, null);
        }

        // draw outline on selected tile
        g2.setColor(Color.RED);
        g2.drawRect(selectedTileIndex * (AppConstants.TILE_SIZE + AppConstants.SELECTOR_BORDER), y,
                AppConstants.TILE_SIZE + AppConstants.SELECTOR_BORDER, AppConstants.TILE_SIZE + AppConstants.SELECTOR_BORDER);
    }

    /** return column and row, wrapped in Pair of tile */
    private Pair<Integer, Integer> getHoveredTileCoordinates(int mouseX, int mouseY) {
        // tile coordinate on grid. Could be 0
        int column = (mouseX + this.worldX - this.screenX) / AppConstants.TILE_SIZE;
        int row = (mouseY + this.worldY - this.screenY) / AppConstants.TILE_SIZE;

        return new Pair<>(column, row);
    }

    /** return index of clicked tile in selector */
    private int getSelectorIndex(int mouseX) {
        return mouseX / (AppConstants.TILE_SIZE + AppConstants.SELECTOR_BORDER);
    }

    private void drawTileCoordinate(Graphics2D g2, int column, int row, int screenX, int screenY) {
        g2.drawString("" + column + ":" + row, screenX, screenY);
    }

    private void drawGrid(Graphics2D g2, int screenX, int screenY) {
        g2.drawRoundRect(screenX, screenY, screenX + AppConstants.TILE_SIZE, screenY + AppConstants.TILE_SIZE, 1, 1);
    }

    public void onScrollUp() {
        selectorTab++;
        if (selectorTab > selectorTabMax)
            selectorTab = 0;
    }

    public void onScrollDown() {
        selectorTab--;
        if (selectorTab < 0)
            selectorTab = selectorTabMax;
    }

    public void loadMap() {
        try {
            map = mapLoader.load("test_map");
        } catch (FileNotFoundException e) {
            log("can not load map. Initializing empty one");
            var tiles = new Tile[50][50];
            //fill with grass
            for (int col = 0; col < tiles.length; col++) {
                for (int row = 0; row < tiles[col].length; row++) {
                    tiles[col][row] = new Tile(TileType.GRASS);
                }
            }
            map = new WorldMap(tiles);
        }
        mapWidth = map.tiles.length;
        mapHeight = map.tiles[0].length;
    }

    public void saveMap() {
        mapLoader.save(map, "test_map");
        log("Map saved");
    }

    private void log(String message) {
        Logger.log(LoggerLayer.MAP_EDITOR, message);
    }
}
