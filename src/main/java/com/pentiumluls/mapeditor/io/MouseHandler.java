package com.pentiumluls.mapeditor.io;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class MouseHandler implements MouseListener {
    public boolean isLeftClicked = false;
    public boolean isMiddleClicked = false;
    public boolean isRightClicked = false;

    @Override
    public void mouseClicked(MouseEvent e) {
        // nothing
    }

    @Override
    public void mousePressed(MouseEvent e) {
        switch (e.getButton()) {
            case MouseEvent.BUTTON1 ->isLeftClicked = true;
            case MouseEvent.BUTTON2 ->isMiddleClicked = true;
            case MouseEvent.BUTTON3 ->isRightClicked = true;
        }
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        switch (e.getButton()) {
            case MouseEvent.BUTTON1 ->isLeftClicked = false;
            case MouseEvent.BUTTON2 ->isMiddleClicked = false;
            case MouseEvent.BUTTON3 ->isRightClicked = false;
        }
    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }
}
