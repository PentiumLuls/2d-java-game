package com.pentiumluls.mapeditor.io;

import com.pentiumluls.mapeditor.MapEditorPanel;

import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;

public class MouseWheelHandler implements MouseWheelListener {
    private MapEditorPanel mapEditorPanel;

    public MouseWheelHandler(MapEditorPanel mapEditorPanel) {
        this.mapEditorPanel = mapEditorPanel;
    }

    @Override
    public void mouseWheelMoved(MouseWheelEvent e) {
        if (e.getWheelRotation() < 0) {
            mapEditorPanel.onScrollUp();
        } else {
            mapEditorPanel.onScrollDown();
        }
    }
}
