package com.pentiumluls.mapeditor.smart_tiling;

import com.pentiumluls.common.data.map.subtype.GrassVariations;
import lombok.AllArgsConstructor;

/** Mapping from Bitmask value of generated tile variation to actual sprite variation */
@AllArgsConstructor
public enum SmartTilingGrassPathMapping {
    GRASS_PATH_0(0, GrassVariations.SMALL_PATH),
    GRASS_PATH_1(2, GrassVariations.SMALL_PATH_VERTICAL_BOTTOM),
    GRASS_PATH_2(8, GrassVariations.SMALL_PATH_HORIZONTAL_RIGHT),
    GRASS_PATH_3(10, GrassVariations.SMALL_CORNERED_BOTTOM_RIGHT),
    GRASS_PATH_4(11, GrassVariations.PATH_BOTTOM_RIGHT),
    GRASS_PATH_5(16, GrassVariations.SMALL_PATH_HORIZONTAL_LEFT),
    GRASS_PATH_6(18, GrassVariations.SMALL_CORNERED_BOTTOM_LEFT),
    GRASS_PATH_7(22, GrassVariations.PATH_BOTTOM_LEFT),
    GRASS_PATH_8(24, GrassVariations.SMALL_PATH_HORIZONTAL),
    GRASS_PATH_9(26, GrassVariations.PATH_BOTTOM),
    GRASS_PATH_10(27, GrassVariations.PATH_BOTTOM),
    GRASS_PATH_11(30, GrassVariations.PATH_BOTTOM),
    GRASS_PATH_12(31, GrassVariations.PATH_BOTTOM),
    GRASS_PATH_13(64, GrassVariations.SMALL_PATH_VERTICAL_TOP),
    GRASS_PATH_14(66, GrassVariations.SMALL_PATH_VERTICAL),
    GRASS_PATH_15(72, GrassVariations.SMALL_CORNERED_TOP_RIGHT),
    GRASS_PATH_16(74, GrassVariations.PATH_MIDDLE_RIGHT),
    GRASS_PATH_17(75, GrassVariations.PATH_MIDDLE_RIGHT),
    GRASS_PATH_18(80, GrassVariations.SMALL_CORNERED_TOP_LEFT),
    GRASS_PATH_19(82, GrassVariations.PATH_MIDDLE_LEFT),
    GRASS_PATH_20(86, GrassVariations.PATH_MIDDLE_LEFT),
    GRASS_PATH_21(88, GrassVariations.PATH_TOP),
    GRASS_PATH_22(90, GrassVariations.PATH_MIDDLE),
    GRASS_PATH_23(91, GrassVariations.SMALL_CORNERED_BIDIRECTIONAL_RIGHT),
    GRASS_PATH_24(94, GrassVariations.SMALL_CORNERED_BIDIRECTIONAL_LEFT),
    GRASS_PATH_25(95, GrassVariations.PATH_MIDDLE),
    GRASS_PATH_26(104, GrassVariations.PATH_TOP_RIGHT),
    GRASS_PATH_27(106, GrassVariations.PATH_MIDDLE_RIGHT),
    GRASS_PATH_28(107, GrassVariations.PATH_MIDDLE_RIGHT),
    GRASS_PATH_29(120, GrassVariations.PATH_TOP),
    GRASS_PATH_30(122, GrassVariations.SMALL_CORNERED_BIDIRECTIONAL_LEFT),
    GRASS_PATH_31(123, GrassVariations.PATH_MIDDLE),
    GRASS_PATH_32(126, GrassVariations.SMALL_CORNERED_BIDIRECTIONAL_LEFT),
    GRASS_PATH_33(127, GrassVariations.PATH_CORNER_TOP_LEFT),
    GRASS_PATH_34(208, GrassVariations.PATH_TOP_LEFT),
    GRASS_PATH_35(210, GrassVariations.PATH_MIDDLE_LEFT),
    GRASS_PATH_36(214, GrassVariations.PATH_MIDDLE_LEFT),
    GRASS_PATH_37(216, GrassVariations.PATH_TOP),
    GRASS_PATH_38(218, GrassVariations.SMALL_CORNERED_BIDIRECTIONAL_RIGHT),
    GRASS_PATH_39(219, GrassVariations.SMALL_CORNERED_BIDIRECTIONAL_RIGHT),
    GRASS_PATH_40(222, GrassVariations.PATH_MIDDLE),
    GRASS_PATH_41(223, GrassVariations.PATH_CORNER_TOP_RIGHT),
    GRASS_PATH_42(248, GrassVariations.PATH_TOP),
    GRASS_PATH_43(250, GrassVariations.PATH_MIDDLE),
    GRASS_PATH_44(251, GrassVariations.PATH_CORNER_BOTTOM_LEFT),
    GRASS_PATH_45(254, GrassVariations.PATH_CORNER_BOTTOM_RIGHT),
    GRASS_PATH_46(255, GrassVariations.PATH_MIDDLE),
    ;

    // { 2 = 1, 8 = 2, 10 = 3, 11 = 4, 16 = 5, 18 = 6, 22 = 7, 24 = 8, 26 = 9, 27 = 10, 30 = 11, 31 = 12, 64 = 13, 66 = 14, 72 = 15, 74 = 16, 75 = 17, 80 = 18,
    // 82 = 19, 86 = 20, 88 = 21, 90 = 22, 91 = 23, 94 = 24, 95 = 25, 104 = 26, 106 = 27, 107 = 28, 120 = 29, 122 = 30, 123 = 31, 126 = 32, 127 = 33,
    // 208 = 34, 210 = 35, 214 = 36, 216 = 37, 218 = 38, 219 = 39, 222 = 40, 223 = 41, 248 = 42, 250 = 43, 251 = 44, 254 = 45, 255 = 46, 0 = 47 }

    public final int bitmaskValue;
    public final GrassVariations grassVariation;

    public static GrassVariations getVariation(int mask) {
        for (SmartTilingGrassPathMapping value : SmartTilingGrassPathMapping.values()) {
            if (value.bitmaskValue == mask)
                return value.grassVariation;
        }
        return null;
    }
}
