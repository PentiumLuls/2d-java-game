package com.pentiumluls.server;

public class AppConstants {
    // SCREEN SETTINGS
    public static final int TPS = 60; // Ticks Per Second
    public static final int TILE_SIZE = 48; // 48x48 pixels each tile

    public static final int DAY_TICKS_AMOUNT = TPS * 60 * 24; // 24 minutes is a whole day

    public static String SAVE_DIR = System.getProperty("user.home") + "/.pentiumluls-server/";
    public static String MAPS_DIR = SAVE_DIR + "maps/";

    public static final String DB_FILE_NAME = "database.db";

}
