package com.pentiumluls.server;

import com.pentiumluls.server.io.DBManager;

public class Core {

    public void start() {
        NetworkLayer networkLayer = new NetworkLayer(6666);
        Thread connectionThread = new Thread(networkLayer);
        connectionThread.start();

        DBManager dbManager = new DBManager();

        DataLayer dataLayer = new DataLayer();

        LogicLayer logicLayer = new LogicLayer(dataLayer);
        Thread logicThread = new Thread(logicLayer);
        logicThread.start();

        EventPool eventPool = new EventPool(networkLayer, logicLayer);
    }
}
