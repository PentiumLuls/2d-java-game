package com.pentiumluls.server;

import com.pentiumluls.common.data.enemy.Enemy;
import com.pentiumluls.common.data.PlayerInventory;
import com.pentiumluls.common.data.map.Tile;
import com.pentiumluls.common.data.map.WorldMap;
import com.pentiumluls.server.data.Player;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Getter
@Setter
public class DataLayer {
    public Map<Integer, Player> players; // Integer = player id
    public Map<Integer, PlayerInventory> playerInventories; // Integer = player id
    public WorldMap map;
    public int mapWidth, mapHeight;

    public Map<Integer, Enemy> enemies; // Integer = enemy id

    // TIME
    public int dayTime; // amount of ticks spend from the beginning of day
    public int dayCount; // amount of days from the beginning of game

    public DataLayer() {
        players = new HashMap<>();
        playerInventories = new HashMap<>();
        enemies = new HashMap<>();
    }

    /** Return currently online players, except for specified player */
    @NotNull
    public List<Player> getOnlinePlayersExceptSpecified(Player player) {
        Player finalPlayer = player;
        List<Player> onlinePlayers = players
                .entrySet()
                .stream()
                .filter(map -> map.getKey() != finalPlayer.getId())
                .map(Map.Entry::getValue)
                .collect(Collectors.toList());
        return onlinePlayers;
    }

    public Player getPlayer(int playerId) {
        return players.get(playerId);
    }

    public List<Player> getPlayers() {
        return players.values().stream().toList();
    }

    public void removePlayer(int playerId) {
        players.remove(playerId);
    }

    public PlayerInventory getPlayerInventory(int playerId) {
        return playerInventories.get(playerId);
    }

    public void removePlayerInventory(int playerId) {
        playerInventories.remove(playerId);
    }

    public Tile[][] tiles() {
        return map.tiles;
    }

    public void addEnemy(Enemy enemy) {
        enemies.put(enemy.getId(), enemy);
    }
}
