package com.pentiumluls.server;

import com.pentiumluls.server.event.Event;

import java.util.ArrayList;
import java.util.List;

public class EventPool {
    private static NetworkLayer networkLayer;
    private static LogicLayer logicLayer;

    private static List<Event> logicEvents = new ArrayList<>();

    public EventPool(NetworkLayer networkLayer, LogicLayer logicLayer) {
        EventPool.networkLayer = networkLayer;
        EventPool.logicLayer = logicLayer;
    }

    /** Add event to pool for later handling in Logic Layer */
    public static void writeLogicEvent(Event event) {
        logicEvents.add(event);
    }

    /** Add event to pool for later handling in Network Layer */
    public static void writeNetworkEvent(Event event) {
        networkLayer.handleEvent(event);
    }

    /** Returns all events from pool for Logic Layer. Empties the pool! */
    public static List<Event> readLogicEvents() {
        List<Event> events_ = new ArrayList<>(logicEvents);
        logicEvents.clear();

        return events_;
    }
}
