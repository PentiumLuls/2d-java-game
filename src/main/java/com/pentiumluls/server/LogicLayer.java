package com.pentiumluls.server;


import com.pentiumluls.common.data.PlayerInventory;
import com.pentiumluls.common.data.item.Item;
import com.pentiumluls.common.data.item.ItemType;
import com.pentiumluls.common.data.object.Object;
import com.pentiumluls.common.exception.CanNotPlaceItemException;
import com.pentiumluls.common.exception.CanNotRemoveInventoryItem;
import com.pentiumluls.common.logic.Command;
import com.pentiumluls.common.util.ItemBuilder;
import com.pentiumluls.common.util.logger.Logger;
import com.pentiumluls.common.util.logger.LoggerLayer;
import com.pentiumluls.server.command.ObjectPickupCommand;
import com.pentiumluls.server.data.Player;
import com.pentiumluls.server.event.Event;
import com.pentiumluls.server.event.logic.*;
import com.pentiumluls.server.event.network.PlaceItemEvent;
import com.pentiumluls.server.event.network.PlayerDisconnectedEvent;
import com.pentiumluls.server.event.network.PlayerJoinedEvent;
import com.pentiumluls.server.event.network.PlayerMovementEvent;
import com.pentiumluls.server.io.PlayerRepository;
import com.pentiumluls.server.logic.*;
import com.pentiumluls.server.logic.helpers.EnemiesLogic;
import com.pentiumluls.server.logic.helpers.ItemConverter;

import java.util.List;

public class LogicLayer implements Runnable {
    private DataLayer dataLayer;
    private PlayersLogic playersLogic;
    private EnemiesLogic enemiesLogic;
    private MapLogic mapLogic;
    private InventoryLogic inventoryLogic;
    private TimeLogic timeLogic;
    private PlayerRepository playerRepository;
    private CollisionDetector collisionDetector;

    private Command objectCollisionCommand = new ObjectPickupCommand(this);

    public LogicLayer(DataLayer dataLayer) {
        this.dataLayer = dataLayer;
        this.playersLogic = new PlayersLogic(dataLayer);
        this.enemiesLogic = new EnemiesLogic(dataLayer);
        this.mapLogic = new MapLogic(dataLayer);
        this.inventoryLogic = new InventoryLogic(dataLayer);
        this.timeLogic = new TimeLogic(dataLayer, this);

        this.playerRepository = new PlayerRepository();
        this.collisionDetector = new CollisionDetector(dataLayer, (ObjectPickupCommand)objectCollisionCommand);
    }

    @Override
    public void run() {
        mapLogic.loadMap();

        double updateInterval = 1000000000 / AppConstants.TPS;
        double delta = 0;
        long lastTime = System.nanoTime();
        long currentTime;
        long timer = 0;
        int updateCount = 0;

        while (true) {
            currentTime = System.nanoTime();
            delta += (currentTime - lastTime) / updateInterval;
            timer += (currentTime - lastTime);
            lastTime = currentTime;

            if (delta >= 1) {
                // ACTUAL GAME LOOP, FRAMED IN PREDEFINED TPS
                update();

                delta--;
                updateCount++;
            }
            if (timer >= 1000000000) {
                saveData(); // Save data every second, instead of every tick
                log("TPS: " + updateCount);
                updateCount = 0;
                timer = 0;
            }
        }
    }

    private void update() {
        timeLogic.update();
        EventPool.readLogicEvents()
                .forEach(this::handleEvent);

        playersLogic.update();
        enemiesLogic.update();
    }

    private void handleEvent(Event e) {
        if (e instanceof PlayerJoinedEvent event) {
            Player player = playerRepository.getPlayer(event.username);

            if (player == null) { // First time joining
                player = playersLogic.createPlayer(event.username);
                player.setId(playerRepository.create(player));
                log("Created new player");
            }

            List<Player> onlinePlayers = dataLayer.getOnlinePlayersExceptSpecified(player);

            playersLogic.playerJoined(player);
            inventoryLogic.initEmptyInventory(player.getId());
            ItemBuilder itemBuilder = new ItemBuilder();
            inventoryLogic.addItem(player.getId(), itemBuilder.of(ItemType.SWORD).build(), 1);
            PlayerInventory inventory = dataLayer.getPlayerInventory(player.getId());
            EventPool.writeNetworkEvent(new PlayerSuccessfullyJoinedEvent(player, onlinePlayers, dataLayer.map, inventory, dataLayer.enemies));

        } else if (e instanceof PlayerMovementEvent event) {
            Player player = dataLayer.getPlayer(event.playerId);
            collisionDetector.checkCollision(player, event.direction);
            playersLogic.playerMoved(player, event.direction);
            EventPool.writeNetworkEvent(new PlayerMovedEvent(player.getId(), player.getDirection(), player.getWorldX(), player.getWorldY()));

        } else if (e instanceof PlayerDisconnectedEvent event) {
            playersLogic.playerDisconnected(event.playerId);

        } else if (e instanceof PlaceItemEvent event) {
            handlePlaceItemEvent(event);
        }
    }

    private void handlePlaceItemEvent(PlaceItemEvent event) {
        Object object;
        try {
            object = ItemConverter.ItemToObject(event.placedItem);
            inventoryLogic.removeItem(event.playerId, event.placedItem, 1);
            mapLogic.placeObject(object, event.worldX, event.worldY);
            EventPool.writeNetworkEvent(new PlaceItemSuccessEvent(event.playerId, event.placedItem, object, event.worldX, event.worldY));
        } catch (CanNotPlaceItemException | CanNotRemoveInventoryItem e) {
            log(e.getMessage());
        }
    }

    /** object was picked up by player */
    public void pickupObject(Player player, Object object, int x, int y) {
        log("Player '" + player.getName() + "' picked up object " + object.name);
        Item pickedItem = null;
        pickedItem = ItemConverter.objectToItem(object);
        int amount = 1;
        inventoryLogic.addItem(player.getId(), pickedItem, amount);
        mapLogic.removeObject(x, y);
        EventPool.writeNetworkEvent(new PlayerPickedObjectEvent(player.getId(), pickedItem, amount, x, y));
    }

    public void newDay() {
        log("NEW DAY: " + dataLayer.dayCount);
        var changedCropObjects = mapLogic.growCrops();
        EventPool.writeNetworkEvent(new NewDayEvent(changedCropObjects));
    }

    /** save all updated and unsaved data */
    private void saveData() {
        // TODO: do bunch update
        // TODO: update only changed data
        log("Save data to DB");
        for (Player player : dataLayer.getPlayers()) {
            playerRepository.update(player);
        }
    }

    private void log(String message) {
        Logger.log(LoggerLayer.LOGIC, message);
    }
}
