package com.pentiumluls.server;

import com.pentiumluls.common.util.logger.Logger;
import com.pentiumluls.common.util.logger.LoggerLayer;
import com.pentiumluls.server.event.Event;
import com.pentiumluls.server.event.logic.*;
import com.pentiumluls.server.net.ClientHandler;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/** responsible for communication with clients */
public class NetworkLayer implements Runnable {

    private int port;
    private ServerSocket serverSocket;

    public NetworkLayer(int port) {
        this.port = port;
        try {
            serverSocket = new ServerSocket(port);
            Logger.log(LoggerLayer.NETWORK, "Server on port " + port + " is started. Waiting for players to connect...");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        start();
    }

    public void start() {
        new Thread(() -> {

            try {
                while (!serverSocket.isClosed()) {
                    Socket socket = serverSocket.accept();
                    ClientHandler clientHandler = new ClientHandler(socket);
                    Thread thread = new Thread(clientHandler);
                    thread.start();
                }

            } catch (IOException e) {
                e.printStackTrace();
                Logger.log(LoggerLayer.NETWORK, "ERROR! Closing server socket");
                closeServerSocket();
            }
        }).start();
    }

    private void closeServerSocket() {
        try {
            if (serverSocket != null)
                serverSocket.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void handleEvent(Event e) {
        if (e instanceof PlayerSuccessfullyJoinedEvent event) {
            ClientHandler.sendMessage(event);

        } else if (e instanceof PlayerMovedEvent event) {
            ClientHandler.sendMessage(event);
        } else if (e instanceof PlayerPickedObjectEvent event) {
            ClientHandler.sendMessage(event);
        } else if (e instanceof PlaceItemSuccessEvent event) {
            ClientHandler.sendMessage(event);
        } else if (e instanceof NewDayEvent event) {
            ClientHandler.sendMessage(event);
        }
    }
}
