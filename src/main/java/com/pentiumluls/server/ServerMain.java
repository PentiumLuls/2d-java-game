package com.pentiumluls.server;

/** Entrypoint to the server part of game */
public class ServerMain {

    public static void main(String[] args) {
        Core core = new Core();
        core.start();
    }
}
