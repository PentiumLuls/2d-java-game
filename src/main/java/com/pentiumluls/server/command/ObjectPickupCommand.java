package com.pentiumluls.server.command;

import com.pentiumluls.common.data.object.Object;
import com.pentiumluls.common.logic.Command;
import com.pentiumluls.server.LogicLayer;
import com.pentiumluls.server.data.Player;

public class ObjectPickupCommand implements Command {
    private LogicLayer logicLayer;
    private Player player;
    private Object object;
    private int x, y;

    public ObjectPickupCommand(LogicLayer logicLayer) {
        this.logicLayer = logicLayer;
    }

    @Override
    public void execute() {
        logicLayer.pickupObject(player, object, x, y);
    }

    public ObjectPickupCommand setPlayer(Player player) {
        this.player = player;
        return this;
    }

    public ObjectPickupCommand setObject(Object object) {
        this.object = object;
        return this;
    }

    public ObjectPickupCommand setCoordinates(int x, int y) {
        this.x = x;
        this.y = y;
        return this;
    }
}
