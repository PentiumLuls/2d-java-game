package com.pentiumluls.server.data;

import com.pentiumluls.common.data.Direction;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class Npc {

    private int id;
    private String name;
    private int worldX, worldY; // in pixels, if you want coordinate of tile on map grid, then multiply by TILE_SIZE
    private int speed;
    private Direction direction = Direction.DOWN;

}
