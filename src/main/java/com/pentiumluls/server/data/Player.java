package com.pentiumluls.server.data;

import com.pentiumluls.common.data.Direction;
import lombok.*;

import java.awt.*;

/** data representation of player. */
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Getter
@Setter
public class Player extends Npc {
    private int speedBonus = 0;
    private boolean collisionOn; // is prevented from moving in current direction
    private Rectangle collisionBox = new Rectangle(15, 28, 22, 36);

    public Player(String name, int worldX, int worldY) {
        setName(name);
        setWorldX(worldX);
        setWorldY(worldY);
        setSpeed(4);
    }

    public Player(int id, String name) {
        setId(id);
        setName(name);
    }

    public int overallSpeed() {
        return getSpeed() + speedBonus;
    }

    public Player(int id, String name, int worldX, int worldY, int speed, int speedBonus, Direction direction) {
        setId(id);
        setName(name);
        setWorldX(worldX);
        setWorldY(worldY);
        setSpeed(speed);
        this.speedBonus = speedBonus;
        setDirection(direction);
    }
}
