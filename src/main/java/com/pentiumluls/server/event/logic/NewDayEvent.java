package com.pentiumluls.server.event.logic;

import com.pentiumluls.common.data.Pair;
import com.pentiumluls.common.data.object.Object;
import com.pentiumluls.server.event.Event;
import lombok.AllArgsConstructor;

import java.util.HashMap;

@AllArgsConstructor
public class NewDayEvent extends Event {
    public HashMap<Pair<Integer, Integer>, Object> changedCropObjects;
}
