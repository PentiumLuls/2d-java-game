package com.pentiumluls.server.event.logic;

import com.pentiumluls.common.data.Direction;
import com.pentiumluls.server.event.Event;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class PlayerMovedEvent extends Event {
    public int playerId;
    public Direction direction;
    public int worldX;
    public int worldY;
}
