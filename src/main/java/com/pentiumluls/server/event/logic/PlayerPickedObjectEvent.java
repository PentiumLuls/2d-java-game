package com.pentiumluls.server.event.logic;

import com.pentiumluls.common.data.item.Item;
import com.pentiumluls.server.event.Event;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class PlayerPickedObjectEvent extends Event {
    public int playerId;
    public Item item;
    public int amount;
    public int x;
    public int y;
}
