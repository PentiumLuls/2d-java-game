package com.pentiumluls.server.event.logic;

import com.pentiumluls.common.data.enemy.Enemy;
import com.pentiumluls.common.data.map.WorldMap;
import com.pentiumluls.common.net.transferdata.PlayerTransferData;
import com.pentiumluls.server.data.Player;
import com.pentiumluls.common.data.PlayerInventory;
import com.pentiumluls.server.event.Event;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/** tells Network layer to send message about successful authorization to player.
 * Also sends current player data, other players, enemies, map */
public class PlayerSuccessfullyJoinedEvent extends Event {
    public PlayerTransferData player;
    public List<PlayerTransferData> onlinePlayers;
    public WorldMap map;
    public PlayerInventory inventory;
    public Map<Integer, Enemy> enemies;

    public PlayerSuccessfullyJoinedEvent(Player player,
                                         List<Player> onlinePlayers,
                                         WorldMap map,
                                         PlayerInventory inventory,
                                         Map<Integer, Enemy> enemies) {
        this.player = new PlayerTransferData(player);

        this.onlinePlayers = onlinePlayers.stream()
                .map(PlayerTransferData::new)
                .collect(Collectors.toList());

        this.map = map;
        this.inventory = inventory;
        this.enemies = enemies;
    }
}
