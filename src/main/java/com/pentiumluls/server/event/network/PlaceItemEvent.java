package com.pentiumluls.server.event.network;

import com.pentiumluls.common.data.item.Item;
import com.pentiumluls.server.event.Event;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class PlaceItemEvent extends Event {
    public int playerId;
    public Item placedItem;
    public int worldX; // tile indexes, not in pixels
    public int worldY;
}
