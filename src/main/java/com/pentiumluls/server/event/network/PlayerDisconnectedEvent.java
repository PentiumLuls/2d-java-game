package com.pentiumluls.server.event.network;

import com.pentiumluls.server.event.Event;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class PlayerDisconnectedEvent extends Event {
    public int playerId;
}
