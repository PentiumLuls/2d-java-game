package com.pentiumluls.server.event.network;

import com.pentiumluls.server.event.Event;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class PlayerJoinedEvent extends Event {

    public String username;
    // TODO: client should send "signup" packet with only username
    public int id; // id = -1 if user is not registered yet.
}
