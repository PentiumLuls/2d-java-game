package com.pentiumluls.server.event.network;

import com.pentiumluls.common.data.Direction;
import com.pentiumluls.server.event.Event;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class PlayerMovementEvent extends Event {
    public int playerId;
    public Direction direction;
}
