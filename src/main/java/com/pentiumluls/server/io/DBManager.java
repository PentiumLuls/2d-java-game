package com.pentiumluls.server.io;

import com.pentiumluls.common.util.logger.Logger;
import com.pentiumluls.common.util.logger.LoggerLayer;
import com.pentiumluls.server.AppConstants;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class DBManager {
    private static Connection connection;

    public DBManager() {
        initDB();
    }

    /**
     * opens connection to DB if not opened. Also, creates DB if not exist.
     * This guarantee that we can open another connection if it closes
     */
    public static Connection getConnection() {
        if (connection != null)
            return connection;

        try {
            Class.forName("org.sqlite.JDBC");
            String dbFileName = AppConstants.DB_FILE_NAME;
            connection = DriverManager.getConnection("jdbc:sqlite:" + dbFileName);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Logger.log(LoggerLayer.IO, "Opened database successfully");
        return connection;
    }

    /**
     * Creates all tables
     */
    public void initDB() {
        try {
            Statement stmt = getConnection().createStatement();
            String sql = "CREATE TABLE IF NOT EXISTS players " +
                    "(id INTEGER PRIMARY KEY AUTOINCREMENT," +
                    " name            TEXT    NOT NULL UNIQUE, " +
                    " world_x         INT     NOT NULL, " +
                    " world_y         INT     NOT NULL," +
                    " speed           INT     NOT NULL," +
                    " speedBonus      INT     NOT NULL," +
                    " direction       TEXT    NOT NULL)";
            stmt.executeUpdate(sql);
            stmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
