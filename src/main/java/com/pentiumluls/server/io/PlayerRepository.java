package com.pentiumluls.server.io;

import com.pentiumluls.common.data.Direction;
import com.pentiumluls.server.data.Player;
import org.jetbrains.annotations.Nullable;

import java.sql.*;

public class PlayerRepository {
    Connection connection = DBManager.getConnection();

    /** return null if not found */
    @Nullable
    public Player getPlayer(int id) {
        return getPlayerByQuery("SELECT * FROM PLAYERS WHERE id=" + id + ";");
    }

    /** return null if not found */
    @Nullable
    public Player getPlayer(String username) {
        return getPlayerByQuery("SELECT * FROM PLAYERS WHERE name='" + username + "';");
    }

    @Nullable
    private Player getPlayerByQuery(String query) {
        Player player = null;
        try {
            Statement stmt = connection.createStatement();
            ResultSet resultSet = stmt.executeQuery(query);

            resultSet.next();
            if (resultSet.isClosed())
                return null;

            player = new Player(
                    resultSet.getInt("id"),
                    resultSet.getString("name"),
                    resultSet.getInt("world_x"),
                    resultSet.getInt("world_y"),
                    resultSet.getInt("speed"),
                    resultSet.getInt("speedBonus"),
                    Direction.fromString(resultSet.getString("direction"))
            );
            resultSet.close();
            stmt.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return player;
    }

    /** save player and return it's generated id */
    public int create(Player player) {
        String sql = "INSERT INTO players (name, world_x, world_y, speed, speedBonus, direction) VALUES('%s',%d,%d,%d,%d,'%s');"
                .formatted(player.getName(), player.getWorldX(), player.getWorldY(), player.getSpeed(), player.getSpeedBonus(), player.getDirection().text);

        try (PreparedStatement statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
            int affectedRows = statement.executeUpdate();

            if (affectedRows == 0)
                throw new SQLException("Creating player failed, no rows affected.");

            try (ResultSet generatedKeys = statement.getGeneratedKeys()) {
                statement.close();
                if (generatedKeys.next()) {
                    return (int) generatedKeys.getLong(1);
                } else {
                    throw new SQLException("Creating user failed, no ID obtained.");
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return -1;
    }

    /** update player */
    public void update(Player player) {
        new Thread(() -> {
            try {
                String query = "UPDATE players SET world_x=?, world_y=?, speed=?, speedBonus=?, direction=? where id=?;";
                PreparedStatement statement = connection.prepareStatement(query);
                statement.setInt(1, player.getWorldX());
                statement.setInt(2, player.getWorldY());
                statement.setInt(3, player.getSpeed());
                statement.setInt(4, player.getSpeedBonus());
                statement.setString(5, player.getDirection().text);
                statement.setInt(6, player.getId());

                statement.executeUpdate();
                statement.close();

            } catch (SQLException e) {
                e.printStackTrace();
            }
        }).start();
    }
}
