package com.pentiumluls.server.io.map;

import com.pentiumluls.common.data.map.WorldMap;
import com.pentiumluls.server.AppConstants;
import com.pentiumluls.common.data.map.Tile;

import java.io.*;

// TODO: create interface
public class MapFileLoader {

    public WorldMap load(String mapName) throws FileNotFoundException {
        createSaveDirectory();

        WorldMap result;
        try {
            FileInputStream is = new FileInputStream(AppConstants.MAPS_DIR + mapName);
            ObjectInputStream ois = new ObjectInputStream(is);
            result = (WorldMap) ois.readObject();
            ois.close();
            is.close();
        } catch (FileNotFoundException e) {
            throw e;
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        }
        return result;
    }

    public void save(WorldMap map, String mapName) {
        createSaveDirectory();
        try {
            FileOutputStream fileOut = new FileOutputStream(AppConstants.MAPS_DIR + mapName);
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(map);
            out.close();
            fileOut.close();
        } catch (IOException i) {
            i.printStackTrace();
        }
    }

    /** create save directory path recursively if not exist */
    private void createSaveDirectory() {
        File directory = new File(AppConstants.MAPS_DIR);
        if (! directory.exists()){
            directory.mkdirs();
        }
    }
}
