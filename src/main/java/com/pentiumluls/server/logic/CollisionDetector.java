package com.pentiumluls.server.logic;

import com.pentiumluls.common.data.Direction;
import com.pentiumluls.common.data.Pair;
import com.pentiumluls.common.data.map.LayeredTile;
import com.pentiumluls.common.data.map.Tile;
import com.pentiumluls.common.data.object.Object;
import com.pentiumluls.common.data.object.PickableObject;
import com.pentiumluls.server.AppConstants;
import com.pentiumluls.server.DataLayer;
import com.pentiumluls.server.command.ObjectPickupCommand;
import com.pentiumluls.server.data.Player;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class CollisionDetector {
    private DataLayer dataLayer;
    private ObjectPickupCommand objectPickupCommand;

    /** If player is colliding with any tile on it's movement direction -
     *  prevent player from moving further in same direction by setting "player.collisionOn" */
    public void checkCollision(Player player, Direction direction) {
        player.setCollisionOn(false); // reset

        int entityLeftWorldX = player.getWorldX() + player.getCollisionBox().x;
        int entityRightWorldX = player.getWorldX() + player.getCollisionBox().x + player.getCollisionBox().width;
        int entityTopWorldY = player.getWorldY() + player.getCollisionBox().y;
        int entityBottomWorldY = player.getWorldY() + player.getCollisionBox().y + player.getCollisionBox().height;

        int entityLeftCol = entityLeftWorldX / AppConstants.TILE_SIZE;
        int entityRightCol = entityRightWorldX / AppConstants.TILE_SIZE;
        int entityTopRow = entityTopWorldY / AppConstants.TILE_SIZE;
        int entityBottomRow = entityBottomWorldY / AppConstants.TILE_SIZE;

        Tile tile1, tile2; // we need to check only 2 tiles in direction of player view
        LayeredTile layeredTile1, layeredTile2; // we need to check only 2 tiles in direction of player view
        Object object1, object2;
        switch (player.getDirection()) {
            case UP -> {
                // check basic tiles
                entityTopRow = (entityTopWorldY - (player.getSpeed() + player.getSpeedBonus())) / AppConstants.TILE_SIZE;
                tile1 = dataLayer.map.tiles[entityLeftCol][entityTopRow];
                tile2 = dataLayer.map.tiles[entityRightCol][entityTopRow];
                if (tile1.type.isCollision() || tile2.type.isCollision()) {
                    player.setCollisionOn(true);
                }
                // check layered tiles
                layeredTile1 = dataLayer.map.layeredTiles.get(new Pair<>(entityLeftCol, entityTopRow));
                layeredTile2 = dataLayer.map.layeredTiles.get(new Pair<>(entityRightCol, entityTopRow));
                if (layeredTile1 != null && layeredTile1.type.isCollision() || layeredTile2 != null && layeredTile2.type.isCollision()) {
                    player.setCollisionOn(true);
                }
                // check objects
                handleObjectCollision(player, entityLeftCol, entityTopRow);
                handleObjectCollision(player, entityRightCol, entityTopRow);
            }
            case DOWN -> {
                // check basic tiles
                entityBottomRow = (entityBottomWorldY + (player.getSpeed() + player.getSpeedBonus())) / AppConstants.TILE_SIZE;
                tile1 = dataLayer.map.tiles[entityLeftCol][entityBottomRow];
                tile2 = dataLayer.map.tiles[entityRightCol][entityBottomRow];
                if (tile1.type.isCollision() || tile2.type.isCollision()) {
                    player.setCollisionOn(true);
                }
                // check layered tiles
                layeredTile1 = dataLayer.map.layeredTiles.get(new Pair<>(entityLeftCol, entityBottomRow));
                layeredTile2 = dataLayer.map.layeredTiles.get(new Pair<>(entityRightCol, entityBottomRow));
                if (layeredTile1 != null && layeredTile1.type.isCollision() || layeredTile2 != null && layeredTile2.type.isCollision()) {
                    player.setCollisionOn(true);
                }
                // check objects
                handleObjectCollision(player, entityLeftCol, entityBottomRow);
                handleObjectCollision(player, entityRightCol, entityBottomRow);
            }
            case LEFT -> {
                // check basic tiles
                entityLeftCol = (entityLeftWorldX - (player.getSpeed() + player.getSpeedBonus())) / AppConstants.TILE_SIZE;
                tile1 = dataLayer.map.tiles[entityLeftCol][entityTopRow];
                tile2 = dataLayer.map.tiles[entityLeftCol][entityBottomRow];
                if (tile1.type.isCollision() || tile2.type.isCollision()) {
                    player.setCollisionOn(true);
                }
                // check layered tiles
                layeredTile1 = dataLayer.map.layeredTiles.get(new Pair<>(entityLeftCol, entityTopRow));
                layeredTile2 = dataLayer.map.layeredTiles.get(new Pair<>(entityLeftCol, entityBottomRow));
                if (layeredTile1 != null && layeredTile1.type.isCollision() || layeredTile2 != null && layeredTile2.type.isCollision()) {
                    player.setCollisionOn(true);
                }
                // check objects
                handleObjectCollision(player, entityLeftCol, entityTopRow);
                handleObjectCollision(player, entityLeftCol, entityBottomRow);
            }
            case RIGHT -> {
                // check basic tiles
                entityRightCol = (entityRightWorldX + (player.getSpeed() + player.getSpeedBonus())) / AppConstants.TILE_SIZE;
                tile1 = dataLayer.map.tiles[entityRightCol][entityTopRow];
                tile2 = dataLayer.map.tiles[entityRightCol][entityBottomRow];
                if (tile1.type.isCollision() || tile2.type.isCollision()) {
                    player.setCollisionOn(true);
                }
                // check layered tiles
                layeredTile1 = dataLayer.map.layeredTiles.get(new Pair<>(entityRightCol, entityTopRow));
                layeredTile2 = dataLayer.map.layeredTiles.get(new Pair<>(entityRightCol, entityBottomRow));
                if (layeredTile1 != null && layeredTile1.type.isCollision() || layeredTile2 != null && layeredTile2.type.isCollision()) {
                    player.setCollisionOn(true);
                }
                // check objects
                handleObjectCollision(player, entityRightCol, entityTopRow);
                handleObjectCollision(player, entityRightCol, entityBottomRow);
            }
        }
    }

    private void handleObjectCollision(Player player, int x, int y) {
        Object object = dataLayer.map.objects.get(new Pair<>(x, y));
        if (object == null)
            return;

        if (object.type.isCollision()) {
            player.setCollisionOn(true);

        } else if (object instanceof PickableObject) {
            objectPickupCommand
                    .setPlayer(player)
                    .setObject(object)
                    .setCoordinates(x, y)
                    .execute();
        }
    }
}
