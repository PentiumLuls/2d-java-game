package com.pentiumluls.server.logic;

import com.pentiumluls.common.data.PlayerInventory;
import com.pentiumluls.common.data.item.Item;
import com.pentiumluls.common.exception.CanNotRemoveInventoryItem;
import com.pentiumluls.server.DataLayer;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class InventoryLogic {
    private DataLayer dataLayer;

    public void removeItem(int playerId, Item item, int amount) throws CanNotRemoveInventoryItem {
        PlayerInventory inventory = dataLayer.playerInventories.get(playerId);
        inventory.removeItem(item, amount);

    }

    public void initEmptyInventory(int playerId) {
        PlayerInventory inventory = new PlayerInventory();
        dataLayer.playerInventories.put(playerId, inventory);
    }

    public void addItem(int playerId, Item item, int amount) {
        dataLayer.playerInventories.get(playerId).addItem(item, amount);
    }
}
