package com.pentiumluls.server.logic;

import com.pentiumluls.common.data.Pair;
import com.pentiumluls.common.data.map.Tile;
import com.pentiumluls.common.data.map.TileType;
import com.pentiumluls.common.data.map.WorldMap;
import com.pentiumluls.common.data.object.Object;
import com.pentiumluls.common.data.object.subtype.CropObject;
import com.pentiumluls.common.util.logger.Logger;
import com.pentiumluls.common.util.logger.LoggerLayer;
import com.pentiumluls.server.DataLayer;
import com.pentiumluls.server.io.map.MapFileLoader;

import java.io.FileNotFoundException;
import java.util.HashMap;

public class MapLogic {
    private DataLayer dataLayer;
    private MapFileLoader mapLoader;

    public MapLogic(DataLayer dataLayer) {
        this.dataLayer = dataLayer;
        this.mapLoader = new MapFileLoader();
    }

    public void loadMap() {
        String mapName = "test_map";
        try {
            dataLayer.setMap(mapLoader.load(mapName));
            log("Loaded map '" + mapName + "'");
        } catch (FileNotFoundException e) {
            dataLayer.setMap(initDefaultMapTiles());
        }
        dataLayer.mapWidth = dataLayer.tiles().length;
        dataLayer.mapHeight = dataLayer.tiles()[0].length;
    }

    public void saveMapTiles(String mapName) {
        mapLoader.save(dataLayer.map, mapName);
    }

    private WorldMap initDefaultMapTiles() {
        log("Init default map...");
        Tile[][] tiles = new Tile[40][40];
        //init grass
        for (int col = 0; col < tiles.length; col++) {
            for (int row = 0; row < tiles[col].length; row++) {
                tiles[col][row] = new Tile(TileType.GRASS);
            }
        }

        return new WorldMap(tiles);
    }

    private void log(String message) {
        Logger.log(LoggerLayer.LOGIC, message);
    }

    public void removeObject(int x, int y) {
        dataLayer.map.objects.remove(new Pair<>(x, y));
    }

    public void placeObject(Object object, int worldX, int worldY) {
        dataLayer.map.objects.put(new Pair<>(worldX, worldY), object);
    }

    /** returns changed crop objects */
    public HashMap<Pair<Integer, Integer>, Object> growCrops() {
        HashMap<Pair<Integer, Integer>, Object> changedObjects = new HashMap<>();
        for (var o : dataLayer.map.objects.entrySet()) {
            if (o.getValue() instanceof CropObject object) {
                object.incrementGrowthStage();
                changedObjects.put(o.getKey(), object);
            }
        }
        return changedObjects;
    }
}
