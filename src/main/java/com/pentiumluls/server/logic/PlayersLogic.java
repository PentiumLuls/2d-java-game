package com.pentiumluls.server.logic;

import com.pentiumluls.common.data.Direction;
import com.pentiumluls.common.data.object.Object;
import com.pentiumluls.common.util.logger.Logger;
import com.pentiumluls.common.util.logger.LoggerLayer;
import com.pentiumluls.server.AppConstants;
import com.pentiumluls.server.DataLayer;
import com.pentiumluls.server.data.Player;
import com.pentiumluls.common.data.PlayerInventory;

/** manages online players and their inventories */
public class PlayersLogic {
    DataLayer dataLayer;

    public PlayersLogic(DataLayer dataLayer) {
        this.dataLayer = dataLayer;
    }

    public void update() {

    }

    public Player createPlayer(String username) {
        return new Player(username, 20 * AppConstants.TILE_SIZE, 10 * AppConstants.TILE_SIZE);
    }

    public void playerJoined(Player player) {
        dataLayer.players.put(player.getId(), player);
    }

    public void playerDisconnected(int playerId) {
        log("player " + dataLayer.getPlayer(playerId).getName() + " disconnected");
        dataLayer.removePlayer(playerId);
        dataLayer.removePlayerInventory(playerId);
    }

    public void playerMoved(Player player, Direction direction) {
        player.setDirection(direction);
        if (player.isCollisionOn())
            return;

        switch (direction) {
            case UP -> player.setWorldY(player.getWorldY() - player.overallSpeed());
            case DOWN -> player.setWorldY(player.getWorldY() + player.overallSpeed());
            case LEFT -> player.setWorldX(player.getWorldX() - player.overallSpeed());
            case RIGHT -> player.setWorldX(player.getWorldX() + player.overallSpeed());
        }
    }

    private void log(String message) {
        Logger.log(LoggerLayer.LOGIC, message);
    }
}
