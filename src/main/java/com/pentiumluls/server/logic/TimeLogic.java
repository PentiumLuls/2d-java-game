package com.pentiumluls.server.logic;

import com.pentiumluls.server.AppConstants;
import com.pentiumluls.server.DataLayer;
import com.pentiumluls.server.LogicLayer;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class TimeLogic {
    private DataLayer dataLayer;
    private LogicLayer logicLayer;

    public void update() {
        dataLayer.dayTime++;
        if (dataLayer.dayTime >= AppConstants.DAY_TICKS_AMOUNT) {
            dataLayer.dayTime = 0;
            dataLayer.dayCount++;
            logicLayer.newDay();
        }
    }
}
