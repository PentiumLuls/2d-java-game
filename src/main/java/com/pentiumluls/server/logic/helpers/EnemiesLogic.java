package com.pentiumluls.server.logic.helpers;

import com.pentiumluls.common.data.enemy.Enemy;
import com.pentiumluls.common.data.enemy.EnemyType;
import com.pentiumluls.server.DataLayer;

/** manages enemies */
public class EnemiesLogic {
    DataLayer dataLayer;

    public EnemiesLogic(DataLayer dataLayer) {
        this.dataLayer = dataLayer;
        dataLayer.addEnemy(new Enemy(0, "Dummy", 1000, 1000, 999, EnemyType.DUMMY));
    }

    /** called in game loop */
    public void update() {

    }
}
