package com.pentiumluls.server.logic.helpers;

import com.pentiumluls.common.data.ObjectToItemMap;
import com.pentiumluls.common.data.item.Item;
import com.pentiumluls.common.data.item.ItemType;
import com.pentiumluls.common.data.object.Object;
import com.pentiumluls.common.data.object.ObjectType;
import com.pentiumluls.common.data.object.subtype.BootsObject;
import com.pentiumluls.common.data.object.subtype.CropObject;
import com.pentiumluls.common.data.object.subtype.KeyObject;
import com.pentiumluls.common.data.object.subtype.SeedsObject;
import com.pentiumluls.common.exception.CanNotPlaceItemException;

/** helps converting items into objects/tiles and vice versa */
public class ItemConverter {

    // TODO: extract names and descriptions
    public static Item objectToItem(Object object) {
        int id;
        String name = object.name;
        String description = "";
        ItemType type;
        if (object instanceof KeyObject o) {
            id = ObjectToItemMap.KEY.itemId;
            description = "Opens doors with doorId=" + o.doorId;
            type = ItemType.KEY;

        } else if (object instanceof BootsObject o) {
            id = ObjectToItemMap.BOOTS.itemId;
            description = "Adds extra " + o.speedBonus + " speed points. Godspeed you brave traveller!";
            type = ItemType.BOOTS;

        } else if (object instanceof SeedsObject o) {
            id = ObjectToItemMap.SEEDS_PARSNIP.itemId;
            description = "Lovely seeds of parsnip. Can be placed in soil";
            type = ItemType.SEED_PARSNIP;

        } else {
            throw new RuntimeException("Can not convert object to item!");
        }

        return new Item(id, 0, name, description, type);
    }

    public static Object ItemToObject(Item item) throws CanNotPlaceItemException {
        if (!item.type.isPlaceAble())
            throw new CanNotPlaceItemException("Can not place item, because it is not placeAble");

        if (item.type == ItemType.SEED_PARSNIP) {
            return new CropObject("parsnip", ObjectType.CROP_PARSNIP);
        }
        return null;
    }
}
