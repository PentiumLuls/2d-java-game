package com.pentiumluls.server.net;

import com.pentiumluls.common.net.packets.*;
import com.pentiumluls.common.util.logger.Logger;
import com.pentiumluls.common.util.logger.LoggerLayer;
import com.pentiumluls.server.EventPool;
import com.pentiumluls.server.data.Player;
import com.pentiumluls.server.event.logic.*;
import com.pentiumluls.server.event.network.PlaceItemEvent;
import com.pentiumluls.server.event.network.PlayerDisconnectedEvent;
import com.pentiumluls.server.event.network.PlayerJoinedEvent;
import com.pentiumluls.server.event.network.PlayerMovementEvent;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;

/** Responsible for connection related to one player.
 * Though, can call other clientHandlers to broadcast message.
 * All clientHandlers stored as static list */
public class ClientHandler implements Runnable {
    public static ArrayList<ClientHandler> clientHandlers = new ArrayList<>();
    private Socket socket;
    private ObjectInputStream inStream;
    private ObjectOutputStream outStream;

    public Player player; // id can be -1 if player joined for the first time

    public ClientHandler(Socket socket) {
        this.socket = socket;
        try {
            inStream = new ObjectInputStream(socket.getInputStream());
            outStream = new ObjectOutputStream(socket.getOutputStream());
            Object packet = inStream.readObject(); // Waiting for "PlayerAuthPacket"
            handlePacket(packet);

        } catch (IOException e) {
            e.printStackTrace();
            disconnect();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
            disconnect();
        }
    }

    @Override
    public void run() {
        while (socket.isConnected()) {
            try {
                Object packet = inStream.readObject(); //listen for messages from client
                handlePacket(packet);

            } catch (IOException e) {
                disconnect();
                break;
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    /** Parse packet from client and write an appropriate event to EventPool */
    private void handlePacket(Object p) {
        if (p instanceof PlayerAuthPacket packet) {
            this.player = new Player(packet.id, packet.username);
            clientHandlers.add(this);
            log("Player " + packet.username + " authorized");
            EventPool.writeLogicEvent(new PlayerJoinedEvent(packet.username, packet.id));

        } else if (p instanceof ChatMessagePacket packet) {
            log(packet.userName + ": " + packet.message);
            broadcastMessage(new ChatMessagePacket(packet.message, packet.userName), false);

        } else if (p instanceof PlayerDisconnectPacket packet) {
            log("Player " + packet.playerId + " has disconnected");
            broadcastMessage(new PlayerDisconnectPacket(packet.playerId), false);
            disconnect();

        } else if (p instanceof PlayerMovePacket packet)
            EventPool.writeLogicEvent(new PlayerMovementEvent(packet.playerId, packet.direction));

        else if (p instanceof PlaceItemPacket packet)
            EventPool.writeLogicEvent(new PlaceItemEvent(player.getId(), packet.placedItem, packet.worldX, packet.worldY));
    }

    public void removeClientHandler() {
        clientHandlers.remove(this);
        if (player != null) {
            log(player.getName() + " disconnected");
            broadcastMessage(new PlayerDisconnectPacket(player.getId()), false);
            EventPool.writeLogicEvent(new PlayerDisconnectedEvent(player.getId()));
        }
    }

    /** send message to all clients, including or not current client(depending on "includeCurrentPlayer" param) */
    public void broadcastMessage(Packet packet, boolean includeCurrentPlayer) {
        for (ClientHandler clientHandler : clientHandlers) {
            if (includeCurrentPlayer || !clientHandler.player.getName().equals(this.player.getName())) {
                try {
                    clientHandler.outStream.writeObject(packet);
                    clientHandler.outStream.flush();
                } catch (IOException e) {
                    e.printStackTrace();
                    disconnect();
                }
            }
        }
    }

    public static void sendMessage(PlayerSuccessfullyJoinedEvent event) {
        try {
            ClientHandler currentClientHandler = currentClientHandler(event.player.name);
            // Notify other users about new player joined
            currentClientHandler.broadcastMessage(new PlayerJoinedPacket(event.player), false);
            // notify joined user about success
            currentClientHandler.outStream.writeObject(new PlayerSuccessfullyJoinedPacket(event.player, event.onlinePlayers, event.map, event.inventory, event.enemies));
            currentClientHandler.outStream.flush();
            currentClientHandler.player.setId(event.player.id);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /** broadcasted to all users */
    public static void sendMessage(PlayerMovedEvent event) {
        sendMessage(new PlayerMovedPacket(event.playerId, event.direction, event.worldX, event.worldY), true);
    }

    /** broadcasted to all users */
    public static void sendMessage(PlayerPickedObjectEvent event) {
        sendMessage(new PlayerPickedObjectPacket(event.playerId, event.item, event.amount, event.x, event.y), true);
    }

    public static void sendMessage(PlaceItemSuccessEvent event) {
        sendMessage(new PlaceItemSuccessPacket(event.playerId, event.item, event.object, event.worldX, event.worldY), true);
    }

    public static void sendMessage(NewDayEvent event) {
        sendMessage(new NewDayPacket(event.changedCropObjects), true);
    }

    public static void sendMessage(Packet packet, boolean includeCurrentPlayer) {
        clientHandlers.stream()
                .findFirst()
                .ifPresentOrElse(
                        (c) -> c.broadcastMessage(packet, includeCurrentPlayer),
                        () -> log("can not send package. No client handlers are present"));
    }

    private void disconnect() {
        log("Disconnecting Client Handler...");
        removeClientHandler();
        try {
            if (inStream != null)
                inStream.close();

            if (outStream != null)
                outStream.close();

            if (socket != null)
                socket.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static ClientHandler currentClientHandler(String playerName) {
        ClientHandler currentClientHandler;
        try {
            currentClientHandler = clientHandlers.stream()
                    .filter(clientHandler -> clientHandler.player.getName().equals(playerName))
                    .findFirst()
                    .orElseThrow(() -> new Exception("Can not find client connection, to send the message"));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return currentClientHandler;
    }

    private static void log(String message) {
        Logger.log(LoggerLayer.NETWORK, message);
    }
}
